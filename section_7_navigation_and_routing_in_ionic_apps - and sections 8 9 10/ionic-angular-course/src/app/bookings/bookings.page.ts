import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  ViewWillEnter,
  ViewWillLeave,
  ViewDidEnter,
  ViewDidLeave,
  MenuController,
  IonItemSliding,
} from "@ionic/angular";
import { MENU_IDS } from "../shared/service/menus.service";
import { Subscription, Observable } from "rxjs";
import { Store } from "@ngrx/store";
import { BookingsState } from "./store/bookings.reducer";
import {
  DeleteBookingAction,
  HttpDeleteBookingAction,
} from "./store/bookings.actions";
import { AppState } from "../store/store";

@Component({
  selector: "app-bookings",
  templateUrl: "./bookings.page.html",
  styleUrls: ["./bookings.page.scss"],
})
export class BookingsPage
  implements
    OnInit,
    ViewWillEnter,
    ViewWillLeave,
    ViewDidEnter,
    ViewDidLeave,
    OnDestroy {
  public bookingsState: Observable<BookingsState>;

  constructor(
    private _menuController: MenuController,
    private _store: Store<AppState>
  ) {}

  public onDeleteBooking = (
    bookingId: string,
    ionItemSliding: IonItemSliding
  ): void => {
    ionItemSliding.close().then(() => {
      this._store.dispatch(new HttpDeleteBookingAction(bookingId));
    });
  };

  ngOnInit() {
    this.bookingsState = this._store.select("bookingsState");
  }

  ionViewWillEnter() {
    // console.log("OffersPage ionViewWillEnter");
  }

  ionViewDidEnter() {
    this._menuController.swipeGesture(true, MENU_IDS.MAIN_MENU);
    // console.log("OffersPage ionViewDidEnter");
  }

  ionViewWillLeave() {
    // console.log("OffersPage ionViewWillLeave");
  }

  ionViewDidLeave() {
    // console.log("OffersPage ionViewDidLeave");
  }

  ngOnDestroy(): void {}
}
