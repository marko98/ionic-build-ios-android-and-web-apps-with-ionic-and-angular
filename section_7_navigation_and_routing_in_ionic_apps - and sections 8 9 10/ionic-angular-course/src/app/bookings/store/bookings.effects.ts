import { Injectable } from "@angular/core";
import { Actions, ofType, Effect } from "@ngrx/effects";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import {
  BOOKINGS_ACTION_TYPES,
  HttpErrorResponseBookingsAction,
  DeleteBookingAction,
} from "./bookings.actions";
import { tap, switchMap, catchError, map } from "rxjs/operators";
import { Action } from "@ngrx/store";
import { BookingModelInterface } from "src/app/shared/model/interface/booking.model-interface";
import { BACKEND_SERVER_URL } from "src/app/shared/consts";
import { of, Observable } from "rxjs";
import {
  SetBookingsAction,
  HttpDeleteBookingActionInterface,
} from "../store/bookings.actions";
import { Booking } from "src/app/shared/model/class/booking.model-class";

const errorHandler = (
  error: HttpErrorResponse
): Observable<HttpErrorResponseBookingsAction> => {
  return of(new HttpErrorResponseBookingsAction(error.message));
};

@Injectable()
export class BookingsEffects {
  @Effect({ dispatch: true })
  fetchBookings = this._actions$.pipe(
    ofType(BOOKINGS_ACTION_TYPES.FETCH_BOOKINGS),
    switchMap((action: Action) => {
      return this._httpClient
        .get<BookingModelInterface[]>(`${BACKEND_SERVER_URL}/bookings`)
        .pipe(
          map((bookings: BookingModelInterface[]) => {
            // console.log(bookings);
            return new SetBookingsAction(
              bookings.map(
                (b) =>
                  new Booking(
                    b.id,
                    b.placeId,
                    b.userId,
                    b.placeTitle,
                    b.guestNumber,
                    new Date(b.dateFrom),
                    new Date(b.dateTo)
                  )
              )
            );
          }),
          catchError(errorHandler)
        );
    })
  );

  @Effect({ dispatch: true })
  httpDeleteBooking = this._actions$.pipe(
    ofType(BOOKINGS_ACTION_TYPES.HTTP_DELETE_BOOKING),
    switchMap((action: HttpDeleteBookingActionInterface) => {
      return this._httpClient
        .delete(`${BACKEND_SERVER_URL}/bookings/${action.bookingId}`)
        .pipe(
          map(() => {
            return new DeleteBookingAction(action.bookingId);
          }),
          catchError(errorHandler)
        );
    })
  );

  constructor(private _actions$: Actions, private _httpClient: HttpClient) {}
}
