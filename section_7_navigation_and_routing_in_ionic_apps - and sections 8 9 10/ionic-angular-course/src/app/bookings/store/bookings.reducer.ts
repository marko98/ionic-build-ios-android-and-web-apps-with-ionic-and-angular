import { Action } from "@ngrx/store";
import { Booking } from "src/app/shared/model/class/booking.model-class";

import {
  BOOKINGS_ACTION_TYPES,
  HttpErrorResponseBookingsActionInterface,
  SetBookingsActionInterface,
  AddBookingActionInterface,
  DeleteBookingActionInterface,
} from "./bookings.actions";

export declare interface BookingsState {
  bookings: Booking[];
  httpErrorResponseMessage: string;
}

const bookingsState: BookingsState = {
  bookings: [],
  httpErrorResponseMessage: undefined,
};

export const bookingsReducer = (
  state: BookingsState = bookingsState,
  action: Action
): BookingsState => {
  // console.log(action);
  switch (action.type) {
    case BOOKINGS_ACTION_TYPES.SET_BOOKINGS:
      return {
        ...state,
        bookings: (action as SetBookingsActionInterface).bookings,
        httpErrorResponseMessage: undefined,
      };
    case BOOKINGS_ACTION_TYPES.HTTP_ERROR_RESPONSE_BOOKINGS:
      return {
        ...state,
        httpErrorResponseMessage: (action as HttpErrorResponseBookingsActionInterface)
          .errorMessage,
      };
    case BOOKINGS_ACTION_TYPES.ADD_BOOKING:
      return {
        ...state,
        bookings: [
          ...state.bookings,
          (action as AddBookingActionInterface).booking,
        ],
        httpErrorResponseMessage: undefined,
      };
    case BOOKINGS_ACTION_TYPES.DELETE_BOOKING:
      return {
        ...state,
        bookings: state.bookings.filter(
          (b) => b.id !== (action as DeleteBookingActionInterface).bookingId
        ),
        httpErrorResponseMessage: undefined,
      };
    default:
      return state;
  }
};
