import { Actions, ofType, Effect } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { of } from "rxjs";
import { switchMap, tap, map, catchError } from "rxjs/operators";

import {
  PLACES_ACTION_TYPES,
  DeletePlaceAction,
  SetPlacesAction,
  HttpDeletePlaceAction,
  HttpErrorResponsePlacesAction,
} from "./places.actions";
import { AppState } from "src/app/store/store";
import { BACKEND_SERVER_URL } from "../../shared/consts";
import { PlaceModelInterface } from "src/app/shared/model/interface/place.model-interface";
import { Place } from "src/app/shared/model/class/place.model-class";

const errorHandler = (error: HttpErrorResponse) => {
  // console.log(error.message);
  return of(new HttpErrorResponsePlacesAction(error.message));
};

@Injectable()
export class PlacesEffects {
  @Effect()
  placeDeleteStart = this._actions$.pipe(
    ofType(PLACES_ACTION_TYPES.HTTP_DELETE_PLACE),
    switchMap((action: HttpDeletePlaceAction) => {
      return this._httpClient
        .delete(BACKEND_SERVER_URL + "/places/" + action.placeId)
        .pipe(
          map(() => {
            return new DeletePlaceAction(action.placeId);
          }),
          catchError(errorHandler)
        );
    })
  );

  @Effect({ dispatch: true })
  fetchPlaces = this._actions$.pipe(
    ofType(PLACES_ACTION_TYPES.FETCH_PLACES),
    switchMap((action: Action) => {
      return this._httpClient
        .get<PlaceModelInterface[]>(BACKEND_SERVER_URL + "/places")
        .pipe(
          map((places: PlaceModelInterface[]) => {
            return new SetPlacesAction(
              places.map(
                (p) =>
                  new Place(
                    p.id,
                    p.title,
                    p.description,
                    p.imageUrl,
                    p.price,
                    new Date(p.dateFrom),
                    new Date(p.dateTo)
                  )
              )
            );
          }),
          catchError(errorHandler)
        );
    })
  );

  constructor(
    private _actions$: Actions,
    private _store: Store<AppState>,
    private _httpClient: HttpClient
  ) {}
}
