import { Action } from "@ngrx/store";
import { Place } from "src/app/shared/model/class/place.model-class";

export const PLACES_ACTION_TYPES = {
  FETCH_PLACES: "[Places Page] Fetch Places",
  HTTP_ERROR_RESPONSE_PLACES: "[Place Page] Http Error Response Places",
  SET_PLACES: "[Places Page] Set Places",

  ADD_PLACE: "[Places Page] Add Place",

  HTTP_DELETE_PLACE: "[Places Page] Http Delete Place",
  DELETE_PLACE: "[Places Page] Delete Place",

  UPDATE_PLACE: "[Places Page] Update Place",
};

// -------------------------------------------------------
export declare interface AddPlaceActionInterface extends Action {
  place: Place;
}
export class AddPlaceAction implements AddPlaceActionInterface {
  readonly type = PLACES_ACTION_TYPES.ADD_PLACE;
  constructor(public place: Place) {}
}
// -------------------------------------------------------
export declare interface HttpDeletePlaceActionInterface extends Action {
  placeId: number;
}
export class HttpDeletePlaceAction implements HttpDeletePlaceActionInterface {
  readonly type = PLACES_ACTION_TYPES.HTTP_DELETE_PLACE;
  constructor(public placeId: number) {}
}
// -------------------------------------------------------
export declare interface DeletePlaceActionInterface extends Action {
  placeId: number;
}
export class DeletePlaceAction implements DeletePlaceActionInterface {
  readonly type = PLACES_ACTION_TYPES.DELETE_PLACE;
  constructor(public placeId: number) {}
}
// -------------------------------------------------------
export declare interface UpdatePlaceActionInterface extends Action {
  place: Place;
}
export class UpdatePlaceAction implements UpdatePlaceActionInterface {
  readonly type = PLACES_ACTION_TYPES.UPDATE_PLACE;
  constructor(public place: Place) {}
}
// -------------------------------------------------------
export declare interface HttpErrorResponsePlacesActionInterface extends Action {
  errorMessage: string;
}
export class HttpErrorResponsePlacesAction
  implements HttpErrorResponsePlacesActionInterface {
  readonly type = PLACES_ACTION_TYPES.HTTP_ERROR_RESPONSE_PLACES;
  constructor(public errorMessage: string) {}
}
// -------------------------------------------------------
export declare interface SetPlacesActionInterface extends Action {
  places: Place[];
}
export class SetPlacesAction implements SetPlacesActionInterface {
  readonly type = PLACES_ACTION_TYPES.SET_PLACES;
  constructor(public places: Place[]) {}
}
