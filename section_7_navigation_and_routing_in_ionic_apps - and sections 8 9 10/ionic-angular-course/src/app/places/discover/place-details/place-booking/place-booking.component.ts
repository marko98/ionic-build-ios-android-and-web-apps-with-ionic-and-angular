import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { Place } from "src/app/shared/model/class/place.model-class";
import { ModalController, IonDatetime } from "@ionic/angular";
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
} from "@angular/forms";

@Component({
  selector: "app-place-booking",
  templateUrl: "./place-booking.component.html",
  styleUrls: ["./place-booking.component.scss"],
})
export class PlaceBookingComponent implements OnInit, OnDestroy {
  @Input() place: Place;
  @Input() mode: "select" | "random" = "select";
  public formGroup: FormGroup;

  constructor(private _modalController: ModalController) {}

  public onDateFromBlur = (): void => {
    let dateTo: AbstractControl = this.formGroup.get("dateTo");
    let dateFrom: AbstractControl = this.formGroup.get("dateFrom");

    if (dateFrom.value && !dateTo) {
      this.formGroup.addControl(
        "dateTo",
        new FormControl(null, {
          updateOn: "blur",
          validators: [Validators.required],
        })
      );
    } else if (dateFrom.value && dateTo && dateFrom.value) {
      if (new Date(dateFrom.value) > new Date(dateTo.value)) {
        this.formGroup.get("dateTo").setValue(undefined);
      }
    }
  };

  public onReduceMsInDate = (date: Date | string, ms: number): Date => {
    if (!(date instanceof Date)) date = new Date(date);

    return new Date(date.getTime() - ms);
  };

  public onAddMsInDate = (date: Date | string, ms: number): Date => {
    if (!(date instanceof Date)) date = new Date(date);

    return new Date(date.getTime() + ms);
  };

  public onSubmit = (): void => {
    if (this.formGroup.valid) {
      console.log(this.formGroup);
    }
  };

  public onModalClose = (role: string): void => {
    this._modalController.dismiss(
      { message: "hello, modal is dismissed" },
      role
    );
  };

  ngOnInit() {
    let dateFrom;
    let dateTo;

    if (this.mode === "random") {
      // console.log(this.place);
      dateFrom = new Date(
        this.place.dateFrom.getTime() +
          Math.random() *
            (this.place.dateTo.getTime() -
              7 * 24 * 60 * 60 * 1000 -
              this.place.dateFrom.getTime())
      );
      dateTo = new Date(
        dateFrom.getTime() + Math.random() * 6 * 24 * 60 * 60 * 1000
      );

      dateFrom = (dateFrom as Date).toISOString();
      dateTo = (dateTo as Date).toISOString();
    }

    this.formGroup = new FormGroup({
      firstName: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      lastName: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      guestNumber: new FormControl(2, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      dateFrom: new FormControl(dateFrom, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
    });

    if (this.mode === "random") {
      this.formGroup.addControl(
        "dateTo",
        new FormControl(dateTo, {
          updateOn: "blur",
          validators: [Validators.required],
        })
      );
    }
    console.log("PlaceBookingComponent init");
  }

  ngOnDestroy() {
    console.log("PlaceBookingComponent destroyed");
  }
}
