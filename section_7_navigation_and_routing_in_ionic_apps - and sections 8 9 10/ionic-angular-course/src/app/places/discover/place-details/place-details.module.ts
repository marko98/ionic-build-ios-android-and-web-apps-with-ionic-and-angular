import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { PlaceDetailsPageRoutingModule } from "./place-details-routing.module";

import { PlaceDetailsPage } from "./place-details.page";
import { PlaceBookingComponent } from "./place-booking/place-booking.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PlaceDetailsPageRoutingModule,
    SharedModule,
  ],
  declarations: [PlaceDetailsPage, PlaceBookingComponent],
  entryComponents: [PlaceBookingComponent],
})
export class PlaceDetailsPageModule {}
