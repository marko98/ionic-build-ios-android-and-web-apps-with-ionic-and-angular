import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  AfterViewInit,
} from "@angular/core";
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from "@angular/cdk/drag-drop";
import { Place } from "src/app/shared/model/class/place.model-class";
import { Subscription } from "rxjs";
import {
  MenuController,
  NavController,
  ViewWillEnter,
  ViewDidEnter,
  ViewWillLeave,
  ViewDidLeave,
} from "@ionic/angular";
import { MENU_IDS } from "src/app/shared/service/menus.service";
import { Store } from "@ngrx/store";
import { PlacesState } from "../store/places.reducer";
import { map } from "rxjs/operators";
import { AppState } from "src/app/store/store";

declare enum ionSegmentValues {
  ALL_PLACES = "allPlaces",
  BOOKABLE_PLACES = "bookablePlaces",
}

@Component({
  selector: "app-discover",
  templateUrl: "./discover.page.html",
  styleUrls: ["./discover.page.scss"],
})
export class DiscoverPage
  implements
    OnInit,
    AfterViewInit,
    ViewWillEnter,
    ViewDidEnter,
    ViewWillLeave,
    ViewDidLeave,
    OnDestroy {
  // todo = [
  //   "Get to work",
  //   "Pick up groceries",
  //   "Go home",
  //   "Fall asleep",
  //   "Get to work",
  //   "Pick up groceries",
  //   "Go home",
  //   "Fall asleep",
  // ];

  // done = [
  //   "Get up",
  //   "Brush teeth",
  //   "Take a shower",
  //   "Check e-mail",
  //   "Walk dog",
  //   "Get up",
  //   "Brush teeth",
  //   "Take a shower",
  //   "Check e-mail",
  //   "Walk dog",
  // ];
  public places: Place[];
  public placesForVirtualScroll: Place[];

  private _placesSubscription: Subscription;

  constructor(
    private _menuController: MenuController,
    private _navController: NavController,
    private _store: Store<AppState>
  ) {}

  // drop(event: CdkDragDrop<string[]>) {
  //   if (event.previousContainer === event.container) {
  //     moveItemInArray(
  //       event.container.data,
  //       event.previousIndex,
  //       event.currentIndex
  //     );
  //   } else {
  //     transferArrayItem(
  //       event.previousContainer.data,
  //       event.container.data,
  //       event.previousIndex,
  //       event.currentIndex
  //     );
  //   }
  // }

  public onIonSegmentChange = (
    event: CustomEvent<{ value: ionSegmentValues }>
  ): void => {
    console.log(event.detail.value);
  };

  public onOpenMenu = (menuId: string): void => {
    // enabling one menu will also automatically disable all the others that are on the same side
    this._menuController
      .enable(true, menuId)
      .then((elMenu: HTMLIonMenuElement) => {
        this._menuController.open(menuId);
      });
  };

  ngOnInit() {
    this._placesSubscription = this._store
      .select("placesState")
      .pipe(map((placesState: PlacesState) => [...placesState.places]))
      .subscribe((places: Place[]) => {
        this.places = places;
        this.placesForVirtualScroll = this.places.splice(1);
      });
    // console.log("DiscoverPage init");
  }

  ngAfterViewInit(): void {
    // console.log("DiscoverPage ngAfterViewInit");
  }

  ionViewWillEnter() {
    // console.log("DiscoverPage ionViewWillEnter");
  }

  ionViewDidEnter() {
    this._menuController.swipeGesture(true, MENU_IDS.MAIN_MENU);
    // console.log("DiscoverPage ionViewDidEnter");
  }

  ionViewWillLeave() {
    // console.log("DiscoverPage ionViewWillLeave");
  }

  ionViewDidLeave() {
    // console.log("DiscoverPage ionViewDidLeave");
  }

  ngOnDestroy(): void {
    if (this._placesSubscription) this._placesSubscription.unsubscribe();
    // console.log("DiscoverPage destroyed");
  }
}
