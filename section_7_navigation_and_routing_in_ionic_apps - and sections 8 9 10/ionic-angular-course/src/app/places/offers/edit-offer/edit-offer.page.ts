import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap } from "@angular/router";
import {
  NavController,
  MenuController,
  ViewWillEnter,
  ViewWillLeave,
  ViewDidEnter,
  ViewDidLeave,
} from "@ionic/angular";
import { Place } from "src/app/shared/model/class/place.model-class";
import { MENU_IDS } from "src/app/shared/service/menus.service";
import { Store } from "@ngrx/store";
import { PlacesState } from "../../store/places.reducer";
import { take, map } from "rxjs/operators";
import { AppState } from "src/app/store/store";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { NavigationService } from "src/app/shared/service/navigation.service";

@Component({
  selector: "app-edit-offer",
  templateUrl: "./edit-offer.page.html",
  styleUrls: ["./edit-offer.page.scss"],
})
export class EditOfferPage
  implements OnInit, ViewWillEnter, ViewWillLeave, ViewDidEnter, ViewDidLeave {
  public formGroup: FormGroup;
  public place: Place;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _navController: NavController,
    private _menuController: MenuController,
    private _store: Store<AppState>,
    private _navigationService: NavigationService
  ) {}

  public onSubmit = (): void => {
    if (this.formGroup.valid) {
      console.log(this.formGroup.value);
      this._navigationService.onNavigateBackward();
    }
  };

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe((paramMap: ParamMap) => {
      if (!paramMap.has("placeId"))
        return this._navigationService.onNavigateBackward();

      this._store
        .select("placesState")
        .pipe(
          take(1),
          map((placesState: PlacesState) => [...placesState.places])
        )
        .subscribe((places: Place[]) => {
          this.place = places.find((p) => p.id === +paramMap.get("placeId"));

          if (!this.place) return this._navigationService.onNavigateBackward();
          this.formGroup = new FormGroup({
            title: new FormControl(this.place.title, {
              updateOn: "blur",
              validators: [Validators.required],
            }),
            description: new FormControl(this.place.description, {
              updateOn: "blur",
              validators: [Validators.required, Validators.maxLength(360)],
            }),
          });
        });
    });
  }

  ionViewWillEnter() {
    // console.log("DiscoverPage ionViewWillEnter");
  }

  ionViewDidEnter() {
    this._menuController.swipeGesture(false, MENU_IDS.MAIN_MENU);
    // console.log("DiscoverPage ionViewDidEnter");
  }

  ionViewWillLeave() {
    // console.log("DiscoverPage ionViewWillLeave");
  }

  ionViewDidLeave() {
    // console.log("DiscoverPage ionViewDidLeave");
  }
}
