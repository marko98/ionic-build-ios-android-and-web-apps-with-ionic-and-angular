import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { IonicModule } from "@ionic/angular";

import { EditOfferPageRoutingModule } from "./edit-offer-routing.module";

import { EditOfferPage } from "./edit-offer.page";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    EditOfferPageRoutingModule,
    SharedModule,
  ],
  declarations: [EditOfferPage],
})
export class EditOfferPageModule {}
