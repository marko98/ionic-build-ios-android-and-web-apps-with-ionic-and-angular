import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { Place } from "src/app/shared/model/class/place.model-class";
import {
  NavController,
  ViewWillEnter,
  ViewWillLeave,
  ViewDidEnter,
  ViewDidLeave,
  MenuController,
} from "@ionic/angular";
import { MENU_IDS } from "src/app/shared/service/menus.service";
import { Store } from "@ngrx/store";
import { PlacesState } from "../../store/places.reducer";
import { map, take } from "rxjs/operators";
import { AppState } from "src/app/store/store";

@Component({
  selector: "app-offer-bookings",
  templateUrl: "./offer-bookings.page.html",
  styleUrls: ["./offer-bookings.page.scss"],
})
export class OfferBookingsPage
  implements OnInit, ViewWillEnter, ViewWillLeave, ViewDidEnter, ViewDidLeave {
  public place: Place;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _navController: NavController,
    private _menuController: MenuController,
    private _store: Store<AppState>
  ) {}

  private _onNavigateBack = (): void => {
    this._navController.navigateBack(["../"]);
  };

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe((paramMap: ParamMap) => {
      if (!paramMap.has("placeId")) return this._onNavigateBack();

      this._store
        .select("placesState")
        .pipe(
          take(1),
          map((placesState: PlacesState) => [...placesState.places])
        )
        .subscribe((places: Place[]) => {
          this.place = places.find((p) => p.id === +paramMap.get("placeId"));

          if (!this.place) return this._onNavigateBack();
        });
    });
  }

  ionViewWillEnter() {
    // console.log("DiscoverPage ionViewWillEnter");
  }

  ionViewDidEnter() {
    this._menuController.swipeGesture(false, MENU_IDS.MAIN_MENU);

    // console.log("DiscoverPage ionViewDidEnter");
  }

  ionViewWillLeave() {
    // console.log("DiscoverPage ionViewWillLeave");
  }

  ionViewDidLeave() {
    // console.log("DiscoverPage ionViewDidLeave");
  }
}
