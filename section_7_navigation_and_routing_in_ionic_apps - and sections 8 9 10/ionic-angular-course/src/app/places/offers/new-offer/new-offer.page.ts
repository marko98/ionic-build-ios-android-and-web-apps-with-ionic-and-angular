import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { IonDatetime } from "@ionic/angular";

@Component({
  selector: "app-new-offer",
  templateUrl: "./new-offer.page.html",
  styleUrls: ["./new-offer.page.scss"],
})
export class NewOfferPage implements OnInit {
  public formGroup: FormGroup;

  constructor() {}

  public onSubmit = (): void => {
    if (this.formGroup.valid) {
      console.log(this.formGroup);
    }
  };

  public setMinForDateTo = (dateFrom: IonDatetime, dateTo: IonDatetime) => {
    if (dateFrom.value) {
      let date = new Date(dateFrom.value);
      // console.log(date);
      date.setHours(date.getHours() + 24);
      // console.log(date);
      dateTo.min = date.toISOString();

      dateTo.value = "";
    }
  };

  ngOnInit() {
    this.formGroup = new FormGroup({
      title: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      description: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.maxLength(360)],
      }),
      price: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      dateFrom: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      dateTo: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
    });
  }
}
