import { Component, OnInit, OnDestroy } from "@angular/core";
import { Place } from "src/app/shared/model/class/place.model-class";
import { Subscription, Observable } from "rxjs";
import {
  NavController,
  ViewWillEnter,
  ViewWillLeave,
  ViewDidEnter,
  ViewDidLeave,
  MenuController,
  IonItemSliding,
} from "@ionic/angular";
import { MENU_IDS } from "src/app/shared/service/menus.service";
import { Store } from "@ngrx/store";
import { PlacesState } from "../store/places.reducer";
import { AppState } from "src/app/store/store";

@Component({
  selector: "app-offers",
  templateUrl: "./offers.page.html",
  styleUrls: ["./offers.page.scss"],
})
export class OffersPage
  implements
    OnInit,
    OnDestroy,
    ViewWillEnter,
    ViewWillLeave,
    ViewDidEnter,
    ViewDidLeave {
  public placesState: Observable<PlacesState>;

  private _placesSubscription: Subscription;

  constructor(
    private _navController: NavController,
    private _menuController: MenuController,
    private _store: Store<AppState>
  ) {}

  ngOnInit() {
    this.placesState = this._store.select("placesState");
  }

  ionViewWillEnter() {
    // console.log("OffersPage ionViewWillEnter");
  }

  ionViewDidEnter() {
    this._menuController.swipeGesture(true, MENU_IDS.MAIN_MENU);
    // console.log("OffersPage ionViewDidEnter");
  }

  ionViewWillLeave() {
    // console.log("OffersPage ionViewWillLeave");
  }

  ionViewDidLeave() {
    // console.log("OffersPage ionViewDidLeave");
  }

  ngOnDestroy() {
    if (this._placesSubscription) this._placesSubscription.unsubscribe();
  }
}
