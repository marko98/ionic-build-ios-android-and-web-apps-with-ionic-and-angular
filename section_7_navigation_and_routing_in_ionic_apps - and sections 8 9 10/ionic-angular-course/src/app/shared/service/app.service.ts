import { Injectable } from "@angular/core";
import { Subject, Observable, BehaviorSubject } from "rxjs";

export enum MODE {
  LIGHT = "LIGHT",
  DARK = "DARK",
}

@Injectable({ providedIn: "root" })
export class AppService {
  private _mode: BehaviorSubject<MODE> = new BehaviorSubject<MODE>(MODE.LIGHT);

  constructor() {}

  public getMode = (): Observable<MODE> => {
    return this._mode.asObservable();
  };

  public toggleMode = (): void => {
    switch (this._mode.getValue()) {
      case MODE.LIGHT:
        this._mode.next(MODE.DARK);
        break;
      case MODE.DARK:
        this._mode.next(MODE.LIGHT);
        break;
    }
    document.body.classList.toggle("dark");
  };
}
