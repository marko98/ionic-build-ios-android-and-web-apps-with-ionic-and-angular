export declare interface PlaceModelInterface {
  id: number;
  title: string;
  description: string;
  imageUrl: string;
  price: number;
  dateFrom: string;
  dateTo: string;
}
