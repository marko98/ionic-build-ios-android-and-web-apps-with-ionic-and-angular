export declare interface BookingModelInterface {
  id: string;
  placeId: string;
  userId: string;
  placeTitle: string;
  guestNumber: number;
  dateFrom: string;
  dateTo: string;
}
