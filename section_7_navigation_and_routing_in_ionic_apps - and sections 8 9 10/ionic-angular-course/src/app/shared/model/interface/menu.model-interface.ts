export declare interface Menu {
  title: string;
  menuId: string;
  items: MenuItem[];
}

export declare interface MenuItem {
  header: string;
  ionIconName: string;
  route: Function;
}
