import {
  Directive,
  ViewContainerRef,
  AfterViewInit,
  Input,
} from "@angular/core";
import { CustomSwipeFunctions } from "../model/interface/custom-swipe-functions.model-interface";

@Directive({
  selector: "[appCustomSwipe]",
})
export class CustomSwipeDirective implements AfterViewInit {
  @Input() customSwipeFunctions: CustomSwipeFunctions;

  private _htmlEl: HTMLElement;
  private _initialX: number = undefined;
  private _initialY: number = undefined;
  private _swipeDirection: "up" | "down" | "left" | "right";
  private _touchEvent: TouchEvent;

  constructor(private _vcRef: ViewContainerRef) {
    this._htmlEl = this._vcRef.element.nativeElement;
  }

  ngAfterViewInit() {
    if (!this.customSwipeFunctions)
      throw new Error("@Input() customSwipeFunctions is required");

    this.customSwipeFunctions.htmlEl = this._htmlEl;

    this._htmlEl.addEventListener(
      "touchstart",
      (touchEvent: TouchEvent) => {
        this._initialX = touchEvent.touches[0].clientX;
        this._initialY = touchEvent.touches[0].clientY;
      },
      false
    );
    this._htmlEl.addEventListener(
      "touchmove",
      (touchEvent: TouchEvent) => {
        if (this._initialX === null) {
          return;
        }

        if (this._initialY === null) {
          return;
        }

        var currentX = touchEvent.touches[0].clientX;
        var currentY = touchEvent.touches[0].clientY;

        var diffX = this._initialX - currentX;
        var diffY = this._initialY - currentY;

        if (Math.abs(diffX) > Math.abs(diffY)) {
          // sliding horizontally
          if (diffX > 0) {
            // swiped left
            // console.log("swiped left");
            this._swipeDirection = "left";
          } else {
            // swiped right
            // console.log("swiped right");
            this._swipeDirection = "right";
          }
        } else {
          // sliding vertically
          if (diffY > 0) {
            // swiped up
            // console.log("swiped up");
            this._swipeDirection = "up";
          } else {
            // swiped down
            // console.log("swiped down");
            this._swipeDirection = "down";
          }
        }

        this._initialX = null;
        this._initialY = null;

        this._touchEvent = touchEvent;
      },
      false
    );

    this._htmlEl.addEventListener(
      "touchend",
      () => {
        if (this._swipeDirection === "up") {
          if (this.customSwipeFunctions.swipeUp) {
            this.customSwipeFunctions.swipeUp.function();
            if (this.customSwipeFunctions.swipeUp.preventDefault)
              this._touchEvent.preventDefault();
          }
        } else if (this._swipeDirection === "down") {
          if (this.customSwipeFunctions.swipeDown) {
            this.customSwipeFunctions.swipeDown.function();
            if (this.customSwipeFunctions.swipeDown.preventDefault)
              this._touchEvent.preventDefault();
          }
        } else if (this._swipeDirection === "left") {
          if (this.customSwipeFunctions.swipeLeft) {
            this.customSwipeFunctions.swipeLeft.function();
            if (this.customSwipeFunctions.swipeLeft.preventDefault)
              this._touchEvent.preventDefault();
          }
        } else if (this._swipeDirection === "right") {
          if (this.customSwipeFunctions.swipeRight) {
            this.customSwipeFunctions.swipeRight.function();
            if (this.customSwipeFunctions.swipeRight.preventDefault)
              this._touchEvent.preventDefault();
          }
        }

        this._swipeDirection = undefined;
        this._touchEvent = undefined;
      },
      false
    );
  }
}
