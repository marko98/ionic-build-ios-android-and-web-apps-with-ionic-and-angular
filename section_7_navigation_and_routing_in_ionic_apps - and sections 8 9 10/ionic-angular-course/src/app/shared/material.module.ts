import { NgModule } from "@angular/core";

import { DragDropModule } from "@angular/cdk/drag-drop";

const material = [DragDropModule];

@NgModule({
  imports: [...material],
  exports: [...material],
})
export class MaterialModule {}
