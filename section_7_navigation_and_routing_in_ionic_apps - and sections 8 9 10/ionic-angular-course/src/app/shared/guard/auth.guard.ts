import { Injectable } from "@angular/core";
import { CanLoad, Route } from "@angular/router";
import { Observable, of } from "rxjs";
import { NavController } from "@ionic/angular";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/store/store";
import { take, map } from "rxjs/operators";
import { AuthState } from "src/app/auth/store/auth.reducer";

@Injectable({ providedIn: "root" })
export class AuthCanLoadGuard implements CanLoad {
  constructor(
    private _navController: NavController,
    private _store: Store<AppState>
  ) {}

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
    return this._store.select("authState").pipe(
      take(1),
      map((authState: AuthState) => {
        if (!authState.loggedIn)
          return this._navController.navigateForward("/auth");

        return true;
      })
    ) as Observable<boolean>;
  }
}
