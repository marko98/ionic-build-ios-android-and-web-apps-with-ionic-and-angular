import { Component, OnInit, OnDestroy } from "@angular/core";

import { Platform } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { MenusService, MENU_IDS } from "./shared/service/menus.service";
import { Menu } from "./shared/model/interface/menu.model-interface";
import { Store } from "@ngrx/store";
import { AppState } from "./store/store";
import { Subscription } from "rxjs";
import { AuthState } from "./auth/store/auth.reducer";
import { NavigationService } from "./shared/service/navigation.service";
import { PLACES_ACTION_TYPES } from "./places/store/places.actions";
import { BOOKINGS_ACTION_TYPES } from "./bookings/store/bookings.actions";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent implements OnInit, OnDestroy {
  public mainMenu: Menu;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private _menusService: MenusService,
    private _store: Store<AppState>,
    private _navigationService: NavigationService
  ) {
    this.initializeApp();
    this._dispatchFetchingActions();
  }

  private _dispatchFetchingActions = (): void => {
    this._store.dispatch({ type: PLACES_ACTION_TYPES.FETCH_PLACES });
    this._store.dispatch({ type: BOOKINGS_ACTION_TYPES.FETCH_BOOKINGS });
  };

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    this.mainMenu = this._menusService.getMenu(MENU_IDS.MAIN_MENU);
    console.log("AppComponent init");
  }

  ionViewWillEnter() {
    // console.log("AppComponent ionViewWillEnter");
  }

  ionViewDidEnter() {
    // console.log("AppComponent ionViewDidEnter");
  }

  ionViewWillLeave() {
    // console.log("AppComponent ionViewWillLeave");
  }

  ionViewDidLeave() {
    // console.log("AppComponent ionViewDidLeave");
  }

  ngOnDestroy() {
    console.log("AppComponent destroyed");
  }
}
