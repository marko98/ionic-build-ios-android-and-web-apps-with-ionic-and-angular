const inputCourseName = document.querySelector("#input-course-name");
const inputCourseRating = document.querySelector("#input-course-rating");
const btnAdd = document.querySelector("#btn-add");
const ionList = document.querySelector("ion-list");

btnAdd.addEventListener("click", () => {
  const courseName = inputCourseName.value;
  const courseRating = inputCourseRating.value;

  if (courseName.trim().length <= 0 || courseRating.trim().length <= 0) {
    return showAlert(
      "Invalid inputs",
      "Please enter valid values for course name and rating"
    );
  } else if (+courseRating <= 0 || +courseRating > 5) {
    return showAlert(
      "Invalid course rating",
      "Course rating valid values: 1 - 5"
    );
  }

  ionList.innerHTML += `<ion-item><ion-label class="ion-text-center"><strong>${
    courseName + " - " + courseRating + "/5"
  }</strong></ion-label></ion-item>`;

  inputCourseName.value = "";
  inputCourseRating.value = "";
});

showAlert = function (header, message) {
  const alert = document.createElement("ion-alert");
  alert.header = header;
  alert.subHeader = "*just an alert*";
  alert.message = message;
  alert.buttons = ["Okay"];

  document.body.appendChild(alert);

  return alert.present();
};
