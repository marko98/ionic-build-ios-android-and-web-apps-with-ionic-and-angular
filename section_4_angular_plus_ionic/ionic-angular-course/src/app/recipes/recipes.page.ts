import { Component, OnInit, OnDestroy } from "@angular/core";
import { Recipe } from "../shared/model/interface/recipe.model-interface";
import { RecipesService } from "../shared/service/recipes.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-recipes",
  templateUrl: "./recipes.page.html",
  styleUrls: ["./recipes.page.scss"],
})
export class RecipesPage implements OnInit, OnDestroy {
  public recipes: Recipe[];

  private _recipesSubscription: Subscription;

  constructor(private _recipesService: RecipesService) {}

  ngOnInit() {
    this._recipesSubscription = this._recipesService.getRecipes().subscribe(
      (recipes: Recipe[]) => {
        this.recipes = recipes;
      },
      (err) => console.log(err)
    );

    console.log("RecipesPage init");
  }

  ngOnDestroy() {
    if (this._recipesSubscription) this._recipesSubscription.unsubscribe();

    console.log("RecipesPage destroyed");
  }
}
