import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { Recipe } from "src/app/shared/model/interface/recipe.model-interface";

@Component({
  selector: "app-recipe-item",
  templateUrl: "./recipe-item.component.html",
  styleUrls: ["./recipe-item.component.scss"],
})
export class RecipeItemComponent implements OnInit, OnDestroy {
  @Input() recipe: Recipe;

  constructor() {}

  ngOnInit() {
    console.log("RecipeItemComponent init");
  }

  ngOnDestroy() {
    console.log("RecipeItemComponent destroyed");
  }
}
