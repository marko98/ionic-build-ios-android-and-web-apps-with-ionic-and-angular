import { Component, OnInit, OnDestroy } from "@angular/core";
import { Recipe } from "src/app/shared/model/interface/recipe.model-interface";
import { RecipesService } from "src/app/shared/service/recipes.service";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { AlertController } from "@ionic/angular";

@Component({
  selector: "app-recipe-details",
  templateUrl: "./recipe-details.page.html",
  styleUrls: ["./recipe-details.page.scss"],
})
export class RecipeDetailsPage implements OnInit, OnDestroy {
  public recipe: Recipe;

  constructor(
    private _recipesService: RecipesService,
    private _activatedRoute: ActivatedRoute,
    private _alertController: AlertController,
    private _router: Router
  ) {}

  public onDeleteRecipe = (): void => {
    this._alertController
      .create({
        header: "Are you sure?",
        message: "Do you want to delete the recipe?",
        buttons: [
          {
            text: "delete",
            handler: () => {
              this._recipesService.deleteRecipe(this.recipe.id);
              this._router.navigate(["../"]);
            },
          },
          { text: "close", role: "close" },
        ],
      })
      .then((alertEl: HTMLIonAlertElement) => {
        alertEl.present();
      });
  };

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe((paramMap: ParamMap) => {
      if (!paramMap.has("recipeId")) {
        // redirect
        return;
      }
      const recipeId = paramMap.get("recipeId");
      this.recipe = this._recipesService.getRecipe(recipeId);
    });

    console.log("RecipeDetailsPage init");
  }

  ngOnDestroy() {
    console.log("RecipeDetailsPage destroyed");
  }
}
