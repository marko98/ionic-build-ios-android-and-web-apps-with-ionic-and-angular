import { Injectable } from "@angular/core";
import { Recipe } from "../model/interface/recipe.model-interface";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({ providedIn: "root" })
export class RecipesService {
  private _recipes: BehaviorSubject<Recipe[]> = new BehaviorSubject<Recipe[]>([
    {
      id: "r1",
      title: "Schnitzel",
      imageUrl: "http://pngimg.com/uploads/schnitzel/schnitzel_PNG12.png",
      ingredients: ["French Fries", "Pork Meat", "Salat"],
    },
    {
      id: "r2",
      title: "Spaghetti",
      imageUrl: "http://pngimg.com/uploads/spaghetti/spaghetti_PNG107.png",
      ingredients: ["Spaghetti", "Meat", "Tomato"],
    },
  ]);

  public getRecipes = (): Observable<Recipe[]> => {
    return this._recipes.asObservable();
  };

  public getRecipe = (recipeId: string): Recipe => {
    return { ...this._recipes.getValue().find((r) => r.id === recipeId) };
  };

  public deleteRecipe = (recipeId: string): void => {
    this._recipes.next(
      this._recipes.getValue().filter((r) => r.id !== recipeId)
    );
  };
}
