const btnClear = document.querySelector("#btn-clear");
const btnAddExpense = document.querySelector("#btn-add-expense");
const inputExpenseReason = document.querySelector("#input-expense-reason");
const inputExpenseAmount = document.querySelector("#input-expense-amount");
const ionList = document.querySelector("ion-list");
const outputTotalExpenses = document.querySelector("#output-total-expenses");

btnClear.addEventListener("click", () => {
  inputExpenseReason.value = "";
  inputExpenseAmount.value = "";
});

btnAddExpense.addEventListener("click", () => {
  const reason = inputExpenseReason.value;
  const amount = inputExpenseAmount.value;

  if (reason.trim().length <= 0 || amount.trim().length <= 0) {
    const alert = document.createElement("ion-alert");
    alert.header = "Invalid inputs";
    alert.subHeader = "*just an alert*";
    alert.message = "Please enter valid reason and amount";
    alert.buttons = ["Okay"];

    document.body.appendChild(alert);

    return alert.present();
  }

  const ionItemText = reason + ": $" + amount;

  const ionItem = document.createElement("ion-item");
  //   ionItem.setAttribute("style", "margin: 0 0 0 25%; width: 50%;");
  ionItem.innerHTML =
    "<ion-label class='ion-text-center'>" + ionItemText + "</ion-label>";

  ionList.appendChild(ionItem);

  let outputList = outputTotalExpenses.innerHTML.split(": $");
  outputTotalExpenses.innerHTML =
    outputList[0] + ": $" + (+outputList[1] + +amount);

  btnClear.click();
});
