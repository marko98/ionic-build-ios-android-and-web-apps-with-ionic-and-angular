import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-chat",
  templateUrl: "./chat.page.html",
  styleUrls: ["./chat.page.scss"],
})
export class ChatPage implements OnInit {
  public meImgSrc: string = "";
  public otherUserImgSrc: string = "";

  constructor() {}

  ngOnInit() {}
}
