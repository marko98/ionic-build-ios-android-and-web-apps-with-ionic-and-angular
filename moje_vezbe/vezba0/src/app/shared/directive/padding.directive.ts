import {
  AfterViewInit,
  Directive,
  Input,
  ViewContainerRef,
} from "@angular/core";

@Directive({
  selector: "[appPadding]",
})
export class PaddingDirective implements AfterViewInit {
  @Input() padding: string = "0px 0px";
  private el: HTMLElement;

  constructor(private _vcref: ViewContainerRef) {
    // console.log(this._vcref.element.nativeElement);
    this.el = this._vcref.element.nativeElement;
  }

  ngAfterViewInit() {
    this.el.style.padding = this.padding;
  }
}
