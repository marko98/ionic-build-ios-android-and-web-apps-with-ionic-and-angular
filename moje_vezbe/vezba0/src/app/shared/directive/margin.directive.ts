import {
  AfterViewInit,
  Directive,
  Input,
  ViewContainerRef,
} from "@angular/core";

@Directive({
  selector: "[appMargin]",
})
export class MarginDirective implements AfterViewInit {
  @Input() margin: string = "0px 0px";
  private el: HTMLElement;

  constructor(private _vcref: ViewContainerRef) {
    // console.log(this._vcref.element.nativeElement);
    this.el = this._vcref.element.nativeElement;
  }

  ngAfterViewInit() {
    this.el.style.margin = this.margin;
  }
}
