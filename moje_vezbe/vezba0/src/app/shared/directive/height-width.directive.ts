import {
  AfterViewInit,
  Directive,
  Input,
  ViewContainerRef,
} from "@angular/core";

@Directive({
  selector: "[appHeightWidth]",
})
export class HeightWidthDirective implements AfterViewInit {
  @Input() height_width: string[] = [];
  private el: HTMLElement;

  constructor(private _vcref: ViewContainerRef) {
    // console.log(this._vcref.element.nativeElement);
    this.el = this._vcref.element.nativeElement;
  }

  ngAfterViewInit() {
    if (this.height_width[0]) this.el.style.height = this.height_width[0];
    if (this.height_width[1]) this.el.style.width = this.height_width[1];
  }
}
