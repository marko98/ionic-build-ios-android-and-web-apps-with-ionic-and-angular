import {
  AfterViewInit,
  Directive,
  Input,
  ViewContainerRef,
} from "@angular/core";

@Directive({
  selector: "[appBoxShadow]",
})
export class BoxShadowDirective implements AfterViewInit {
  // https://www.cssmatic.com/box-shadow
  // horizontal length, vertical length, blur radius, spread radius
  @Input() boxShadow: string = "0px 0px 0px 0px rgba(0, 0, 0, 0)";
  private el: HTMLElement;

  constructor(private _vcref: ViewContainerRef) {
    // console.log(this._vcref.element.nativeElement);
    this.el = this._vcref.element.nativeElement;
  }

  ngAfterViewInit() {
    this.el.style.boxShadow = this.boxShadow;
  }
}
