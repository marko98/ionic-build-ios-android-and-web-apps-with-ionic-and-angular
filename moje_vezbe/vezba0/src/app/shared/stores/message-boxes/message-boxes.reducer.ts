import { Action } from "@ngrx/store";
import { MessageBoxClassInterface } from "../../model/class-interface/message-box.class-interface";

export declare interface MessageBoxesState {
  messageBoxes: MessageBoxClassInterface[];
}

const messageBoxesState: MessageBoxesState = {
  messageBoxes: [],
};

export const messageBoxReducer = (
  state: MessageBoxesState = messageBoxesState,
  action: Action
): MessageBoxesState => {
  state = { ...state };
  switch (action.type) {
    default:
      return state;
  }
};
