import { Action } from "@ngrx/store";
import { MessageClassInterface } from "../../model/class-interface/message.class-interface";

export declare interface MessagesState {
  messages: MessageClassInterface[];
}

const messagesState: MessagesState = {
  messages: [],
};

export const messageReducer = (
  state: MessagesState = messagesState,
  action: Action
): MessagesState => {
  state = { ...state };
  switch (action.type) {
    default:
      return state;
  }
};
