import { ActionReducerMap } from "@ngrx/store";
import {
  MessageBoxesState,
  messageBoxReducer,
} from "../message-boxes/message-boxes.reducer";
import { messageReducer, MessagesState } from "../messages/messages.reducer";
import { usersReducer, UsersState } from "../users/users.reducer";

export declare interface AppState {
  usersState: UsersState;
  messagesState: MessagesState;
  messageBoxesState: MessageBoxesState;
}

export const appStore: ActionReducerMap<AppState> = {
  usersState: usersReducer,
  messagesState: messageReducer,
  messageBoxesState: messageBoxReducer,
};
