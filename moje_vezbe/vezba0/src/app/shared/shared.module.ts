import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { MarginDirective } from "./directive/margin.directive";
import { MaterialModule } from "./material.module";
import { FlexLayoutModule } from "@angular/flex-layout";
import { PaddingDirective } from "./directive/padding.directive";
import { HeightWidthDirective } from "./directive/height-width.directive";
import { BoxShadowDirective } from "./directive/box-shadow.directive";

@NgModule({
  declarations: [
    MarginDirective,
    PaddingDirective,
    HeightWidthDirective,
    BoxShadowDirective,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    MaterialModule,
    FlexLayoutModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    MaterialModule,
    FlexLayoutModule,

    MarginDirective,
    PaddingDirective,
    HeightWidthDirective,
    BoxShadowDirective,
  ],
  entryComponents: [],
})
export class SharedModule {}
