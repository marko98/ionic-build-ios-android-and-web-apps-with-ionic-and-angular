import { NgModule } from "@angular/core";

import { MatSliderModule } from "@angular/material/slider";
import { MatRippleModule } from "@angular/material/core";

const material = [MatSliderModule, MatRippleModule];

@NgModule({
  imports: [...material],
  exports: [...material],
})
export class MaterialModule {}
