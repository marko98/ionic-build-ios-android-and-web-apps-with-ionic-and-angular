import { MessageBoxClassInterface } from "./message-box.class-interface";
import { UserClassInterface } from "./user.class-interface";

export declare interface MessageClassInterface {
  from: UserClassInterface;
  to: MessageBoxClassInterface;
  context: string;
  dateUTCISOString: string;
}
