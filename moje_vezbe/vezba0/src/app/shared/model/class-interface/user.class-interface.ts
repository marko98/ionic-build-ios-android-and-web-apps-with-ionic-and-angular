export declare interface UserClassInterface {
  id: number;
  username: string;
  imgSrc: string;
}
