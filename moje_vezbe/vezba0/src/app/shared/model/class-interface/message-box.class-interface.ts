import { MessageClassInterface } from "./message.class-interface";
import { UserClassInterface } from "./user.class-interface";

export declare interface MessageBoxClassInterface {
  observers: UserClassInterface[];
  messages: MessageClassInterface[];
  owner: UserClassInterface;
}
