import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { NavigationService } from "src/app/shared/service/navigation.service";
import { AUTH_ACTION_TYPES } from "./auth.actions";
import { tap } from "rxjs/operators";
import { Action } from "@ngrx/store";

@Injectable()
export class AuthEffects {
  @Effect({ dispatch: false })
  logout = this._actions$.pipe(
    ofType(AUTH_ACTION_TYPES.LOGOUT),
    tap((action: Action) => {
      this._navigationService.onNavigateRoot("/auth");
    })
  );

  @Effect({ dispatch: false })
  login = this._actions$.pipe(
    ofType(AUTH_ACTION_TYPES.LOGIN),
    tap((action: Action) => {
      this._navigationService.onNavigateRoot("/places");
    })
  );

  constructor(
    private _actions$: Actions,
    private _navigationService: NavigationService
  ) {}
}
