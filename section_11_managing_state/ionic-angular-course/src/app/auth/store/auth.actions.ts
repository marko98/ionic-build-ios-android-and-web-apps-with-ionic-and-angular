export const AUTH_ACTION_TYPES = {
  LOGIN: "[Auth Page] Login",
  LOGOUT: "[Auth Page] Logout",
};
