import { Action } from "@ngrx/store";
import { AUTH_ACTION_TYPES } from "./auth.actions";

export declare interface AuthState {
  loggedIn: boolean;
  userId: number;
}

const authState: AuthState = {
  loggedIn: true,
  userId: 0,
};

export const authReducer = (
  state: AuthState = authState,
  action: Action
): AuthState => {
  switch (action.type) {
    case AUTH_ACTION_TYPES.LOGIN:
      return { ...state, loggedIn: true };
    case AUTH_ACTION_TYPES.LOGOUT:
      return { ...state, loggedIn: false };
    default:
      return state;
  }
};
