import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  ViewWillEnter,
  ViewDidEnter,
  ViewWillLeave,
  ViewDidLeave,
  NavController,
  MenuController,
  LoadingController,
} from "@ionic/angular";
import { MENU_IDS } from "../shared/service/menus.service";
import { Store } from "@ngrx/store";
import { AppState } from "../store/store";
import { AUTH_ACTION_TYPES } from "./store/auth.actions";
import { Subscription } from "rxjs";
import { AuthState } from "./store/auth.reducer";
import { NavigationService } from "../shared/service/navigation.service";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-auth",
  templateUrl: "./auth.page.html",
  styleUrls: ["./auth.page.scss"],
})
export class AuthPage
  implements
    OnInit,
    ViewWillEnter,
    ViewDidEnter,
    ViewWillLeave,
    ViewDidLeave,
    OnDestroy {
  public isLogging: boolean = true;
  public isLoading: boolean = false;

  constructor(
    private _menuController: MenuController,
    private _loadingController: LoadingController,
    private _store: Store<AppState>,
    private _navigationService: NavigationService
  ) {}

  public onSubmit = (form: NgForm): void => {
    if (form.valid) {
      if (this.isLogging) {
        // login
        this._loadingController
          .create({ message: "Please wait...", keyboardClose: true })
          .then((ionLoadingEl: HTMLIonLoadingElement) => {
            this.isLoading = true;
            ionLoadingEl.present();
            setTimeout(() => {
              ionLoadingEl.dismiss();
              ionLoadingEl.onDidDismiss().then((e) => {
                this.isLoading = false;
                this._store.dispatch({ type: AUTH_ACTION_TYPES.LOGIN });
              });
            }, 1500);
          });
      } else {
        // sign up
      }
      console.log(form.value);
    }
  };

  ngOnInit() {}

  ionViewWillEnter() {
    // console.log("AuthPage ionViewWillEnter");
  }

  ionViewDidEnter() {
    this._menuController.swipeGesture(false, MENU_IDS.MAIN_MENU);
    // console.log("AuthPage ionViewDidEnter");
  }

  ionViewWillLeave() {
    // console.log("AuthPage ionViewWillLeave");
  }

  ionViewDidLeave() {
    // console.log("AuthPage ionViewDidLeave");
  }

  ngOnDestroy(): void {}
}
