import { ActionReducerMap } from "@ngrx/store";
import { PlacesState, placesReducer } from "../places/store/places.reducer";
import {
  BookingsState,
  bookingsReducer,
} from "../bookings/store/bookings.reducer";
import { AuthState, authReducer } from "../auth/store/auth.reducer";

export interface AppState {
  placesState: PlacesState;
  bookingsState: BookingsState;
  authState: AuthState;
}

export const appActionReducerMap: ActionReducerMap<AppState> = {
  placesState: placesReducer, // ove funkcije vracaju tipove koje zahteva interfejs
  bookingsState: bookingsReducer,
  authState: authReducer,
};
