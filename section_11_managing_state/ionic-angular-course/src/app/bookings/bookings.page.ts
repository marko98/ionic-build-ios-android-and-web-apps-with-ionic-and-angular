import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  ViewWillEnter,
  ViewWillLeave,
  ViewDidEnter,
  ViewDidLeave,
  MenuController,
  IonItemSliding,
  AlertController,
} from "@ionic/angular";
import { MENU_IDS } from "../shared/service/menus.service";
import { Subscription, Observable } from "rxjs";
import { Store } from "@ngrx/store";
import { BookingsState } from "./store/bookings.reducer";
import {
  BOOKINGS_ACTION_TYPES,
  DeleteBookingAction,
  HttpDeleteBookingAction,
} from "./store/bookings.actions";
import { AppState } from "../store/store";
import { tap } from "rxjs/operators";

@Component({
  selector: "app-bookings",
  templateUrl: "./bookings.page.html",
  styleUrls: ["./bookings.page.scss"],
})
export class BookingsPage
  implements
    OnInit,
    ViewWillEnter,
    ViewWillLeave,
    ViewDidEnter,
    ViewDidLeave,
    OnDestroy {
  public bookingsState: Observable<BookingsState>;

  private _alertEl: HTMLIonAlertElement;

  constructor(
    private _menuController: MenuController,
    private _store: Store<AppState>,
    private _alertController: AlertController
  ) {
    this._dispatchFetchingActions();
  }

  private _dispatchFetchingActions = (): void => {
    this._store.dispatch({ type: BOOKINGS_ACTION_TYPES.FETCH_BOOKINGS });
  };

  public onDeleteBooking = (
    bookingId: string,
    ionItemSliding: IonItemSliding
  ): void => {
    ionItemSliding.close().then(() => {
      this._store.dispatch(new HttpDeleteBookingAction(bookingId));
    });
  };

  ngOnInit() {
    this.bookingsState = this._store.select("bookingsState").pipe(
      tap((bookingsState: BookingsState) => {
        if (bookingsState.httpErrorResponseMessage) {
          this._alertController
            .create({
              header: "Error occurred.",
              message: bookingsState.httpErrorResponseMessage,
              buttons: [
                {
                  text: "Okay",
                  handler: () => {
                    if (this._alertEl) {
                      this._alertEl.dismiss().then(() => {
                        this._alertEl = undefined;
                      });
                    }
                  },
                },
              ],
            })
            .then((alertEl: HTMLIonAlertElement) => {
              this._alertEl = alertEl;
              this._alertEl.present();
            });
        }
      })
    );
  }

  ionViewWillEnter() {
    // console.log("OffersPage ionViewWillEnter");
  }

  ionViewDidEnter() {
    this._menuController.swipeGesture(true, MENU_IDS.MAIN_MENU);
    // console.log("OffersPage ionViewDidEnter");
  }

  ionViewWillLeave() {
    // console.log("OffersPage ionViewWillLeave");
  }

  ionViewDidLeave() {
    // console.log("OffersPage ionViewDidLeave");
  }

  ngOnDestroy(): void {}
}
