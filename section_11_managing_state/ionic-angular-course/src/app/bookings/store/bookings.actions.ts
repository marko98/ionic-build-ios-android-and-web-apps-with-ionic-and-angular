import { Action } from "@ngrx/store";
import { Booking } from "src/app/shared/model/class/booking.model-class";
import { BookingModelInterface } from "src/app/shared/model/interface/booking.model-interface";

export const BOOKINGS_ACTION_TYPES = {
  FETCH_BOOKINGS: "[Bookings Page] Fetch Bookings",
  SET_BOOKINGS: "[Bookings Page] Set Bookings",

  HTTP_POST_BOOKING: "[Bookings Page] Http Post Booking",
  ADD_BOOKING: "[Bookings Page] Add Booking",

  HTTP_DELETE_BOOKING: "[Bookings Page] Http Delete Booking",
  DELETE_BOOKING: "[Bookings Page] Delete Booking",

  UPDATE_BOOKING: "[Bookings Page] Update Booking",

  HTTP_ERROR_RESPONSE_BOOKINGS: "[Booknigs Page] Http Error Response Bookings",
};
// ----------------------------------------------------------------------
export declare interface SetBookingsActionInterface extends Action {
  bookings: Booking[];
}
export class SetBookingsAction implements SetBookingsActionInterface {
  readonly type: string = BOOKINGS_ACTION_TYPES.SET_BOOKINGS;
  constructor(public bookings: Booking[]) {}
}
// ----------------------------------------------------------------------
export declare interface HttpPostBookingActionInterface extends Action {
  bookingModelInterface: BookingModelInterface;
}
export class HttpPostBookingAction implements HttpPostBookingActionInterface {
  readonly type: string = BOOKINGS_ACTION_TYPES.HTTP_POST_BOOKING;
  constructor(public bookingModelInterface: BookingModelInterface) {}
}
// ----------------------------------------------------------------------
export declare interface AddBookingActionInterface extends Action {
  booking: Booking;
}
export class AddBookingAction implements AddBookingActionInterface {
  readonly type: string = BOOKINGS_ACTION_TYPES.ADD_BOOKING;
  constructor(public booking: Booking) {}
}
// ----------------------------------------------------------------------
export declare interface HttpDeleteBookingActionInterface extends Action {
  bookingId: string;
}
export class HttpDeleteBookingAction
  implements HttpDeleteBookingActionInterface {
  readonly type: string = BOOKINGS_ACTION_TYPES.HTTP_DELETE_BOOKING;
  constructor(public bookingId: string) {}
}
// ----------------------------------------------------------------------
export declare interface DeleteBookingActionInterface extends Action {
  bookingId: number;
}
export class DeleteBookingAction implements DeleteBookingActionInterface {
  readonly type: string = BOOKINGS_ACTION_TYPES.DELETE_BOOKING;
  constructor(public bookingId: number) {}
}
// ----------------------------------------------------------------------
export declare interface UpdateBookingActionInterface extends Action {
  booking: Booking;
}
export class UpdateBookingAction implements UpdateBookingActionInterface {
  readonly type: string = BOOKINGS_ACTION_TYPES.UPDATE_BOOKING;
  constructor(public booking: Booking) {}
}
// ----------------------------------------------------------------------
export declare interface HttpErrorResponseBookingsActionInterface
  extends Action {
  errorMessage: string;
}
export class HttpErrorResponseBookingsAction
  implements HttpErrorResponseBookingsActionInterface {
  readonly type: string = BOOKINGS_ACTION_TYPES.HTTP_ERROR_RESPONSE_BOOKINGS;
  constructor(public errorMessage: string) {}
}
