export declare interface BookingModelInterface {
  id?: number;
  placeId: number;
  userId: number;
  userFirstName: string;
  userLastName: string;
  placeTitle: string;
  guestNumber: number;
  dateFromUTCISOString: string;
  dateToUTCISOString: string;
  placeImgUrl: string;
}
