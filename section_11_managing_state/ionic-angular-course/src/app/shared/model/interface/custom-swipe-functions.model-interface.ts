export declare interface CustomSwipeFunctions {
  htmlEl?: HTMLElement;
  swipeUp?: CustomSwipeFunction;
  swipeDown?: CustomSwipeFunction;
  swipeLeft?: CustomSwipeFunction;
  swipeRight?: CustomSwipeFunction;
}

export declare interface CustomSwipeFunction {
  function: Function;
  preventDefault: boolean;
}
