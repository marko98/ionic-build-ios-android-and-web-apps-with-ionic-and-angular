export declare interface PlaceModelInterface {
  id?: number;
  title: string;
  description: string;
  imageUrl?: string;
  price: number;
  dateFromUTCISOString: string;
  dateToUTCISOString: string;
  userId: number;
}
