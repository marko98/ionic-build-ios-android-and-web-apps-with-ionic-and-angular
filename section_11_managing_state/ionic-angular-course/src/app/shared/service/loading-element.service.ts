import { Injectable } from "@angular/core";
import { LoadingController } from "@ionic/angular";
import { from, Observable, of, throwError } from "rxjs";
import { switchMap, tap } from "rxjs/operators";

@Injectable({ providedIn: "root" })
export class LoadingElementService {
  private _ionLoadingElement: HTMLIonLoadingElement;

  constructor(private _loadingController: LoadingController) {}

  public onPresentLoadingElement = (
    message: string,
    keyboardClose: boolean = true
  ): Observable<any> => {
    return from(
      this._loadingController.create({
        message: message,
        keyboardClose: keyboardClose,
      })
    ).pipe(
      switchMap((ionLoadingElement: HTMLIonLoadingElement) => {
        if (!this._ionLoadingElement) {
          this._ionLoadingElement = ionLoadingElement;
          return from(this._ionLoadingElement.present());
        } else {
          return throwError({ message: "_ionLoadingElement exists already" });
        }
      })
    );
  };

  public onDismissLoadingElement = (): Observable<any> => {
    if (this._ionLoadingElement) {
      return from(this._ionLoadingElement.dismiss()).pipe(
        tap(() => {
          this._ionLoadingElement = undefined;
        })
      );
    } else {
      return throwError({ message: "_ionLoadingElement doesn't exist" });
    }
  };
}
