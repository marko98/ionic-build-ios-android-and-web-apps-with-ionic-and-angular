import { Injectable } from "@angular/core";
import { Menu } from "../model/interface/menu.model-interface";
import { NavigationService } from "./navigation.service";
import { AppService, MODE } from "./app.service";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/store/store";
import { AUTH_ACTION_TYPES } from "src/app/auth/store/auth.actions";

export enum MENU_IDS {
  MAIN_MENU = "MAIN_MENU",
}

@Injectable({ providedIn: "root" })
export class MenusService {
  private _menus: { [key: string]: Menu } = {};
  private _mode: MODE;

  constructor(
    private _navigationService: NavigationService,
    private _appService: AppService,
    private _store: Store<AppState>
  ) {
    this._loadMenus();

    this._appService.getMode().subscribe((mode: MODE) => {
      this._mode = mode;

      this._menus[MENU_IDS.MAIN_MENU].items[2].header =
        this._mode === MODE.LIGHT ? "Dark Theme" : "Light Theme";
    });
  }

  public getMenu = (menuId: MENU_IDS): Menu => {
    return this._menus[menuId];
  };

  private _loadMenus = (): void => {
    this._menus[MENU_IDS.MAIN_MENU] = {
      title: "PairBnB",
      menuId: MENU_IDS.MAIN_MENU,
      items: [
        {
          header: "Discover Places",
          ionIconName: "business-outline",
          route: () => {
            this._navigationService.onNavigateRoot("/places/tabs/discover");
          },
        },
        {
          header: "Your Bookings",
          ionIconName: "checkmark-circle-outline",
          route: () => {
            this._navigationService.onNavigateRoot("/bookings");
          },
        },
        {
          header: "Toggle Theme",
          ionIconName: "toggle-outline",
          route: () => {
            this._appService.toggleMode();
          },
        },
        {
          header: "Logout",
          ionIconName: "exit-outline",
          route: () => {
            this._store.dispatch({ type: AUTH_ACTION_TYPES.LOGOUT });
          },
        },
      ],
    };
  };
}
