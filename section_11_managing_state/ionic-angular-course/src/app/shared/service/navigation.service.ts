import { Injectable } from "@angular/core";
import { NavController } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/store/store";
import { AuthState } from "../../auth/store/auth.reducer";

@Injectable({ providedIn: "root" })
export class NavigationService {
  constructor(
    private _navController: NavController,
    private _store: Store<AppState>
  ) {
    // this._store.select("authState").subscribe((authState: AuthState) => {
    //   if (authState.loggedIn) this.onNavigateRoot("/places");
    //   else this.onNavigateRoot("/auth");
    // });
  }

  public onNavigateForward = (
    url: string | any[],
    activatedRoute?: ActivatedRoute
  ): void => {
    this._navController.navigateForward(url, { relativeTo: activatedRoute });
  };

  public onNavigateBackward = (url?: string): void => {
    if (!url) return this._navController.back();
    this._navController.navigateBack(url);
  };

  public onNavigateRoot = (url: string): void => {
    this._navController.navigateRoot(url);
  };
}
