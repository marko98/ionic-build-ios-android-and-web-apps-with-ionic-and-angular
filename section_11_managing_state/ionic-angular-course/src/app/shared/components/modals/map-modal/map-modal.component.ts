import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
} from "@angular/core";
import { ModalController } from "@ionic/angular";
import { GOOGLE_API_KEY } from "src/app/shared/consts";

@Component({
  selector: "app-map-modal",
  templateUrl: "./map-modal.component.html",
  styleUrls: ["./map-modal.component.scss"],
})
export class MapModalComponent implements OnInit, AfterViewInit {
  @ViewChild("map") public mapDivRef: ElementRef;

  constructor(
    private _modalController: ModalController,
    private _renderer2: Renderer2
  ) {}

  public onModalClose = (): void => {
    this._modalController.dismiss();
  };

  private _onLoadGoogleMapsModule = (): Promise<any> => {
    const win = window as any;
    const googleModule = win.google;
    if (googleModule && googleModule.maps) {
      return Promise.resolve(googleModule.maps);
    }
    return new Promise((resolve, reject) => {
      const scriptEl: HTMLScriptElement = document.createElement("script");
      scriptEl.src = `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_API_KEY}`;
      scriptEl.async = true;
      scriptEl.defer = true;
      document.body.appendChild(scriptEl);

      scriptEl.onload = () => {
        const loadedGoogleModule = win.google;
        if (loadedGoogleModule && loadedGoogleModule.maps) {
          resolve(loadedGoogleModule.maps);
        } else {
          reject("Google Maps SDK not available");
        }
      };
    });
  };

  ngOnInit() {}

  ngAfterViewInit() {
    // console.log(this.mapDivRef.nativeElement);

    this._onLoadGoogleMapsModule()
      .then((googleMaps) => {
        // console.log(googleMaps);
        const mapDiv = this.mapDivRef.nativeElement;
        const map = new googleMaps.Map(mapDiv, {
          center: {
            lat: 40.7835892,
            lng: -73.9596581,
          },
          zoom: 14,
        });

        googleMaps.event.addListenerOnce(map, "idle", () => {
          this._renderer2.addClass(mapDiv, "visible");
        });
      })
      .catch((err) => console.log(err));
  }
}
