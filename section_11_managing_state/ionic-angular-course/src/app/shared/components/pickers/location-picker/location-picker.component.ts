import { Component, OnInit } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { from } from "rxjs";
import { take } from "rxjs/operators";
import { MapModalComponent } from "../../modals/map-modal/map-modal.component";

@Component({
  selector: "app-location-picker",
  templateUrl: "./location-picker.component.html",
  styleUrls: ["./location-picker.component.scss"],
})
export class LocationPickerComponent implements OnInit {
  constructor(private _modalController: ModalController) {}

  public onModalOpen = () => {
    from(
      this._modalController.create({
        component: MapModalComponent,
        keyboardClose: true,
      })
    )
      .pipe(take(1))
      .subscribe((modalEl: HTMLIonModalElement) => {
        modalEl.present();
      });
  };

  ngOnInit() {}
}
