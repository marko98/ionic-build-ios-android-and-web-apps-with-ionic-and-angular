import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "./material.module";
import { FlexLayoutModule } from "@angular/flex-layout";
import { CustomSwipeDirective } from "./directive/custom-swipe.directive";
import { ToLocalDatePipe } from "./pipe/to-local-date.pipe";
import { LocationPickerComponent } from "./components/pickers/location-picker/location-picker.component";
import { MapModalComponent } from "./components/modals/map-modal/map-modal.component";
import { IonicModule } from "@ionic/angular";

@NgModule({
  declarations: [
    CustomSwipeDirective,
    ToLocalDatePipe,
    LocationPickerComponent,
    MapModalComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    IonicModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,

    CustomSwipeDirective,

    ToLocalDatePipe,

    LocationPickerComponent,
  ],
  entryComponents: [MapModalComponent],
})
export class SharedModule {}
