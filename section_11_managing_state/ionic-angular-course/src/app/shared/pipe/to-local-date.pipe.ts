import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "toLocalDate",
})
export class ToLocalDatePipe implements PipeTransform {
  transform(UTCISOString: string): Date {
    return new Date(UTCISOString);
  }
}
