import { Action } from "@ngrx/store";
import { Place } from "src/app/shared/model/class/place.model-class";
import { PlaceModelInterface } from "src/app/shared/model/interface/place.model-interface";

export const PLACES_ACTION_TYPES = {
  FETCH_PLACES: "[Places Page] Fetch Places",
  HTTP_ERROR_RESPONSE_PLACES: "[Place Page] Http Error Response Places",
  SET_PLACES: "[Places Page] Set Places",

  HTTP_FETCH_PLACE: "[Places Page] Fetch Place",
  SET_THE_PLACE: "[Place Page] Set The Place",

  HTTP_ADD_PLACE: "[Places Page] Http Add Place",
  ADD_PLACE: "[Places Page] Add Place",

  HTTP_DELETE_PLACE: "[Places Page] Http Delete Place",
  DELETE_PLACE: "[Places Page] Delete Place",

  HTTP_UPDATE_PLACE: "[Places Page] Http Update Place",
  UPDATE_PLACE: "[Places Page] Update Place",
};

// -------------------------------------------------------
export declare interface HttpFetchPlaceActionInterface extends Action {
  placeId: number;
  redirectTo?: string;
}
export class HttpFetchPlaceAction implements HttpFetchPlaceActionInterface {
  readonly type = PLACES_ACTION_TYPES.HTTP_FETCH_PLACE;
  constructor(public placeId: number, public redirectTo?: string) {}
}
// -------------------------------------------------------
export declare interface SetThePlaceActionInterface extends Action {
  thePlace: Place;
}
export class SetThePlaceAction implements SetThePlaceActionInterface {
  readonly type = PLACES_ACTION_TYPES.SET_THE_PLACE;
  constructor(public thePlace: Place) {}
}
// -------------------------------------------------------
export declare interface AddPlaceActionInterface extends Action {
  place: Place;
  redirectTo?: string;
}
export class AddPlaceAction implements AddPlaceActionInterface {
  readonly type = PLACES_ACTION_TYPES.ADD_PLACE;
  constructor(public place: Place, public redirectTo?: string) {}
}
// -------------------------------------------------------
export declare interface HttpAddPlaceActionInterface extends Action {
  placeModelInterface: PlaceModelInterface;
}
export class HttpAddPlaceAction implements HttpAddPlaceActionInterface {
  readonly type = PLACES_ACTION_TYPES.HTTP_ADD_PLACE;
  constructor(public placeModelInterface: PlaceModelInterface) {}
}
// -------------------------------------------------------
export declare interface HttpDeletePlaceActionInterface extends Action {
  placeId: number;
}
export class HttpDeletePlaceAction implements HttpDeletePlaceActionInterface {
  readonly type = PLACES_ACTION_TYPES.HTTP_DELETE_PLACE;
  constructor(public placeId: number) {}
}
// -------------------------------------------------------
export declare interface DeletePlaceActionInterface extends Action {
  placeId: number;
}
export class DeletePlaceAction implements DeletePlaceActionInterface {
  readonly type = PLACES_ACTION_TYPES.DELETE_PLACE;
  constructor(public placeId: number) {}
}
// -------------------------------------------------------
export declare interface HttpUpdatePlaceActionInterface extends Action {
  placeModelInterface: PlaceModelInterface;
}

export class HttpUpdatePlaceAction implements HttpUpdatePlaceActionInterface {
  readonly type = PLACES_ACTION_TYPES.HTTP_UPDATE_PLACE;
  constructor(public placeModelInterface: PlaceModelInterface) {}
}
// -------------------------------------------------------
export declare interface UpdatePlaceActionInterface extends Action {
  place: Place;
  redirectTo?: string;
}
export class UpdatePlaceAction implements UpdatePlaceActionInterface {
  readonly type = PLACES_ACTION_TYPES.UPDATE_PLACE;
  constructor(public place: Place, public redirectTo?: string) {}
}
// -------------------------------------------------------
export declare interface HttpErrorResponsePlacesActionInterface extends Action {
  errorMessage: string;
}
export class HttpErrorResponsePlacesAction
  implements HttpErrorResponsePlacesActionInterface {
  readonly type = PLACES_ACTION_TYPES.HTTP_ERROR_RESPONSE_PLACES;
  constructor(public errorMessage: string) {}
}
// -------------------------------------------------------
export declare interface SetPlacesActionInterface extends Action {
  places: Place[];
}
export class SetPlacesAction implements SetPlacesActionInterface {
  readonly type = PLACES_ACTION_TYPES.SET_PLACES;
  constructor(public places: Place[]) {}
}
