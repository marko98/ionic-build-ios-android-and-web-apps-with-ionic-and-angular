import { Actions, ofType, Effect } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { from, of } from "rxjs";
import {
  switchMap,
  tap,
  map,
  catchError,
  take,
  takeUntil,
} from "rxjs/operators";

import {
  PLACES_ACTION_TYPES,
  DeletePlaceAction,
  SetPlacesAction,
  HttpDeletePlaceAction,
  HttpErrorResponsePlacesAction,
  HttpAddPlaceAction,
  AddPlaceAction,
  HttpUpdatePlaceAction,
  UpdatePlaceAction,
  HttpFetchPlaceAction,
  SetThePlaceAction,
} from "./places.actions";
import { AppState } from "src/app/store/store";
import { BACKEND_SERVER_URL } from "../../shared/consts";
import { PlaceModelInterface } from "src/app/shared/model/interface/place.model-interface";
import { Place } from "src/app/shared/model/class/place.model-class";
import { AuthState } from "src/app/auth/store/auth.reducer";
import { title } from "process";
import { NavigationService } from "src/app/shared/service/navigation.service";
import { LoadingElementService } from "src/app/shared/service/loading-element.service";

@Injectable()
export class PlacesEffects {
  @Effect()
  httpDeletePlace = this._actions$.pipe(
    ofType(PLACES_ACTION_TYPES.HTTP_DELETE_PLACE),
    switchMap((action: HttpDeletePlaceAction) => {
      return this._httpClient
        .delete(BACKEND_SERVER_URL + "/places/" + action.placeId)
        .pipe(
          map(() => {
            return new DeletePlaceAction(action.placeId);
          }),
          catchError((err: HttpErrorResponse) => {
            return this._errorHandler(err);
          })
        );
    })
  );

  @Effect({ dispatch: true })
  fetchPlaces = this._actions$.pipe(
    ofType(PLACES_ACTION_TYPES.FETCH_PLACES),
    switchMap((action: Action) => {
      return this._loadingElementService
        .onPresentLoadingElement("Fetching places...")
        .pipe(
          map(() => {
            return action;
          }),
          catchError((err: HttpErrorResponse) => {
            return this._errorHandler(err);
          })
        );
    }),
    switchMap((action: Action) => {
      return this._httpClient
        .get<PlaceModelInterface[]>(BACKEND_SERVER_URL + "/places")
        .pipe(
          switchMap((places: PlaceModelInterface[]) => {
            return this._loadingElementService.onDismissLoadingElement().pipe(
              map(() => {
                return places;
              }),
              catchError((err: HttpErrorResponse) => {
                return this._errorHandler(err);
              })
            );
          }),
          map((places: PlaceModelInterface[]) => {
            return new SetPlacesAction(
              places.map(
                (p) =>
                  new Place(
                    p.id,
                    p.title,
                    p.description,
                    p.imageUrl,
                    p.price,
                    new Date(p.dateFromUTCISOString),
                    new Date(p.dateToUTCISOString),
                    p.userId
                  )
              )
            );
          }),
          catchError((err: HttpErrorResponse) => {
            return this._errorHandler(err, "Fetching places failed.");
          })
        );
    })
  );

  @Effect({ dispatch: true })
  httpAddPlace = this._actions$.pipe(
    ofType(PLACES_ACTION_TYPES.HTTP_ADD_PLACE),
    switchMap((action: HttpAddPlaceAction) => {
      return this._loadingElementService
        .onPresentLoadingElement("Creating new offer(place)...")
        .pipe(
          map(() => {
            return action;
          }),
          catchError((err: HttpErrorResponse) => {
            return this._errorHandler(err);
          })
        );
    }),
    switchMap((action: HttpAddPlaceAction) => {
      return this._httpClient
        .post<PlaceModelInterface>(
          `${BACKEND_SERVER_URL}/places`,
          action.placeModelInterface
        )
        .pipe(
          map((placeModelInterface: PlaceModelInterface) => {
            return new AddPlaceAction(
              new Place(
                placeModelInterface.id,
                placeModelInterface.title,
                placeModelInterface.description,
                placeModelInterface.imageUrl,
                placeModelInterface.price,
                new Date(placeModelInterface.dateFromUTCISOString),
                new Date(placeModelInterface.dateToUTCISOString),
                placeModelInterface.userId
              ),
              "/places/tabs/offers"
            );
          }),
          catchError((err: HttpErrorResponse) => {
            return this._errorHandler(err);
          })
        );
    })
  );

  @Effect({ dispatch: false })
  addPlace = this._actions$.pipe(
    ofType(PLACES_ACTION_TYPES.ADD_PLACE),
    tap((action: AddPlaceAction) => {
      // console.log(action);
      this._loadingElementService
        .onDismissLoadingElement()
        .pipe(
          tap(() => {
            if (action.redirectTo)
              this._navigationService.onNavigateBackward(action.redirectTo);
          }),
          catchError((err: HttpErrorResponse) => {
            return this._errorHandler(err);
          })
        )
        .subscribe();
    })
  );

  @Effect({ dispatch: true })
  httpUpdatePlace = this._actions$.pipe(
    ofType(PLACES_ACTION_TYPES.HTTP_UPDATE_PLACE),
    switchMap((action: HttpUpdatePlaceAction) => {
      return this._loadingElementService
        .onPresentLoadingElement("Updating place...")
        .pipe(
          map(() => {
            return action;
          }),
          catchError((err: HttpErrorResponse) => {
            return this._errorHandler(err);
          })
        );
    }),
    switchMap((action: HttpUpdatePlaceAction) => {
      return this._httpClient
        .put<PlaceModelInterface>(
          `${BACKEND_SERVER_URL}/places`,
          action.placeModelInterface
        )
        .pipe(
          map((placeModelInterface: PlaceModelInterface) => {
            return new UpdatePlaceAction(
              new Place(
                placeModelInterface.id,
                placeModelInterface.title,
                placeModelInterface.description,
                placeModelInterface.imageUrl,
                placeModelInterface.price,
                new Date(placeModelInterface.dateFromUTCISOString),
                new Date(placeModelInterface.dateToUTCISOString),
                placeModelInterface.userId
              ),
              "/places/tabs/offers"
            );
          }),
          catchError((err: HttpErrorResponse) => {
            return this._errorHandler(err);
          })
        );
    })
  );

  @Effect({ dispatch: false })
  updatePlace = this._actions$.pipe(
    ofType(PLACES_ACTION_TYPES.UPDATE_PLACE),
    tap((action: UpdatePlaceAction) => {
      this._loadingElementService
        .onDismissLoadingElement()
        .pipe(
          tap(() => {
            if (action.redirectTo)
              this._navigationService.onNavigateBackward(action.redirectTo);
          }),
          catchError((err: HttpErrorResponse) => {
            return this._errorHandler(err);
          })
        )
        .subscribe();
    })
  );

  @Effect({ dispatch: true })
  httpFetchPlace = this._actions$.pipe(
    ofType(PLACES_ACTION_TYPES.HTTP_FETCH_PLACE),
    switchMap((action: HttpFetchPlaceAction) => {
      return this._loadingElementService
        .onPresentLoadingElement("Fetching the place...")
        .pipe(
          map(() => {
            return action;
          }),
          catchError((err) => this._errorHandler(err))
        );
    }),
    switchMap((action: HttpFetchPlaceAction) => {
      return this._httpClient
        .get<PlaceModelInterface>(
          `${BACKEND_SERVER_URL}/places/${action.placeId}`
        )
        .pipe(
          map((placeModelInterface: PlaceModelInterface) => {
            return new SetThePlaceAction(
              new Place(
                placeModelInterface.id,
                placeModelInterface.title,
                placeModelInterface.description,
                placeModelInterface.imageUrl,
                placeModelInterface.price,
                new Date(placeModelInterface.dateFromUTCISOString),
                new Date(placeModelInterface.dateToUTCISOString),
                placeModelInterface.userId
              )
            );
          }),
          catchError((err) => this._errorHandler(err))
        );
    })
  );

  @Effect({ dispatch: false })
  setThePlace = this._actions$.pipe(
    ofType(PLACES_ACTION_TYPES.SET_THE_PLACE),
    tap(() => {
      this._loadingElementService
        .onDismissLoadingElement()
        .pipe(
          take(1),
          catchError((err) => this._errorHandler(err))
        )
        .subscribe();
    })
  );

  constructor(
    private _actions$: Actions,
    private _store: Store<AppState>,
    private _httpClient: HttpClient,
    private _navigationService: NavigationService,
    private _loadingElementService: LoadingElementService
  ) {}

  private _errorHandler = (
    error: HttpErrorResponse,
    msg: string = error.message
  ) => {
    console.log(error);
    // console.log(error.message);
    // console.log(msg);

    return this._loadingElementService.onDismissLoadingElement().pipe(
      map(() => {
        return new HttpErrorResponsePlacesAction(msg);
      }),
      catchError(() => {
        console.log(
          "cudno zasto se ovde ne dispatch-uje akcija sama tj od strane ngrx-a?"
        );
        this._store.dispatch(new HttpErrorResponsePlacesAction(msg));
        return of();
      })
    );
  };
}
