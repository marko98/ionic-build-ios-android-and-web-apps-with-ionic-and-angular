import { Place } from "src/app/shared/model/class/place.model-class";
import { Action } from "@ngrx/store";
import {
  PLACES_ACTION_TYPES,
  AddPlaceActionInterface,
  UpdatePlaceActionInterface,
  DeletePlaceActionInterface,
  SetPlacesActionInterface,
  HttpErrorResponsePlacesActionInterface,
  SetThePlaceAction,
} from "./places.actions";

export declare interface PlacesState {
  places: Place[];
  fetchedThePlace: Place;
  httpErrorResponseMessage: string;
}

const placesState: PlacesState = {
  places: [],
  fetchedThePlace: undefined,
  httpErrorResponseMessage: undefined,
};

export const placesReducer = (
  state: PlacesState = placesState,
  action: Action
): PlacesState => {
  // console.log(action);
  switch (action.type) {
    case PLACES_ACTION_TYPES.ADD_PLACE:
      return {
        ...state,
        places: [...state.places, (action as AddPlaceActionInterface).place],
        httpErrorResponseMessage: undefined,
        fetchedThePlace: undefined,
      };

    case PLACES_ACTION_TYPES.UPDATE_PLACE:
      return {
        ...state,
        places: [...state.places].map((p) => {
          if (p.id === (action as UpdatePlaceActionInterface).place.id) {
            return (action as UpdatePlaceActionInterface).place;
          }
          return p;
        }),
        httpErrorResponseMessage: undefined,
        fetchedThePlace: undefined,
      };
    case PLACES_ACTION_TYPES.DELETE_PLACE:
      return {
        ...state,
        places: state.places.filter(
          (p) => p.id !== (action as DeletePlaceActionInterface).placeId
        ),
        httpErrorResponseMessage: undefined,
        fetchedThePlace: undefined,
      };

    case PLACES_ACTION_TYPES.SET_PLACES:
      return {
        ...state,
        places: (action as SetPlacesActionInterface).places,
        fetchedThePlace: undefined,
      };

    case PLACES_ACTION_TYPES.HTTP_ERROR_RESPONSE_PLACES:
      console.log(
        (action as HttpErrorResponsePlacesActionInterface).errorMessage
      );
      return {
        ...state,
        httpErrorResponseMessage: (action as HttpErrorResponsePlacesActionInterface)
          .errorMessage,
        fetchedThePlace: undefined,
      };

    case PLACES_ACTION_TYPES.SET_THE_PLACE:
      return {
        ...state,
        fetchedThePlace: (action as SetThePlaceAction).thePlace,
      };

    default:
      return state;
  }
};
