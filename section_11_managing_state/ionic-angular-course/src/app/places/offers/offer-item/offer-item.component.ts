import { Component, OnInit, Input } from "@angular/core";
import { Place } from "src/app/shared/model/class/place.model-class";
import { IonItemSliding } from "@ionic/angular";
import { NavigationService } from "src/app/shared/service/navigation.service";
import { ActivatedRoute } from "@angular/router";
import { Store } from "@ngrx/store";
import { PlacesState } from "../../store/places.reducer";
import {
  DeletePlaceAction,
  HttpDeletePlaceAction,
} from "../../store/places.actions";
import { AppState } from "src/app/store/store";

@Component({
  selector: "app-offer-item",
  templateUrl: "./offer-item.component.html",
  styleUrls: ["./offer-item.component.scss"],
})
export class OfferItemComponent implements OnInit {
  @Input() place: Place;

  constructor(
    private _navigationService: NavigationService,
    private _activatedRoute: ActivatedRoute,
    private _store: Store<AppState>
  ) {}

  public onEdit = (placeId: string, ionItemSliding: IonItemSliding): void => {
    ionItemSliding.close().then(() => {
      this._navigationService.onNavigateForward(
        ["edit", placeId],
        this._activatedRoute
      );
    });
  };

  public onDelete = (placeId: string, ionItemSliding: IonItemSliding): void => {
    ionItemSliding.close().then(() => {
      // console.log("you should delete place with id", placeId);
      this._store.dispatch(new HttpDeletePlaceAction(+placeId));
    });
  };

  ngOnInit() {}
}
