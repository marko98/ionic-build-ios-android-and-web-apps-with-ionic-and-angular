import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { IonicModule } from "@ionic/angular";

import { OffersPageRoutingModule } from "./offers-routing.module";

import { OffersPage } from "./offers.page";
import { OfferItemComponent } from "./offer-item/offer-item.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  imports: [CommonModule, IonicModule, OffersPageRoutingModule, SharedModule],
  declarations: [OffersPage, OfferItemComponent],
})
export class OffersPageModule {}
