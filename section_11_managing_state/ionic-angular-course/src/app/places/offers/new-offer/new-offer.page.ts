import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { IonDatetime, LoadingController } from "@ionic/angular";
import { Store } from "@ngrx/store";
import { take } from "rxjs/operators";

import { AuthState } from "src/app/auth/store/auth.reducer";
import { AppState } from "src/app/store/store";
import { HttpAddPlaceAction } from "../../store/places.actions";

@Component({
  selector: "app-new-offer",
  templateUrl: "./new-offer.page.html",
  styleUrls: ["./new-offer.page.scss"],
})
export class NewOfferPage implements OnInit {
  public formGroup: FormGroup;

  constructor(private _store: Store<AppState>) {}

  public onSubmit = (): void => {
    if (this.formGroup.valid) {
      // console.log(this.formGroup);

      this._store
        .select("authState")
        .pipe(take(1))
        .subscribe((authState: AuthState) => {
          this._store.dispatch(
            new HttpAddPlaceAction({
              title: this.formGroup.value.title,
              description: this.formGroup.value.description,
              price: +this.formGroup.value.price,
              dateFromUTCISOString: new Date(
                new Date(this.formGroup.value.dateFrom).toUTCString()
              ).toISOString(),
              dateToUTCISOString: new Date(
                new Date(this.formGroup.value.dateTo).toUTCString()
              ).toISOString(),
              userId: authState.userId,
            })
          );
        });
    }
  };

  public setMinForDateTo = (dateFrom: IonDatetime, dateTo: IonDatetime) => {
    if (dateFrom.value) {
      let date = new Date(dateFrom.value);
      // console.log(date);
      date.setHours(date.getHours() + 24);
      // console.log(date);
      dateTo.min = date.toISOString();

      dateTo.value = "";
    }
  };

  ngOnInit() {
    this.formGroup = new FormGroup({
      title: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      description: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.maxLength(360)],
      }),
      price: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      dateFrom: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      dateTo: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
    });
  }
}
