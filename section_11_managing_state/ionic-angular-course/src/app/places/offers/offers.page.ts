import { Component, OnInit, OnDestroy } from "@angular/core";
import { Place } from "src/app/shared/model/class/place.model-class";
import { Subscription, Observable } from "rxjs";
import {
  NavController,
  ViewWillEnter,
  ViewWillLeave,
  ViewDidEnter,
  ViewDidLeave,
  MenuController,
  IonItemSliding,
  AlertController,
} from "@ionic/angular";
import { MENU_IDS } from "src/app/shared/service/menus.service";
import { Store } from "@ngrx/store";
import { PlacesState } from "../store/places.reducer";
import { AppState } from "src/app/store/store";
import { PLACES_ACTION_TYPES } from "../store/places.actions";
import { map, tap } from "rxjs/operators";

@Component({
  selector: "app-offers",
  templateUrl: "./offers.page.html",
  styleUrls: ["./offers.page.scss"],
})
export class OffersPage
  implements
    OnInit,
    OnDestroy,
    ViewWillEnter,
    ViewWillLeave,
    ViewDidEnter,
    ViewDidLeave {
  public places: Place[] = [];
  public isLoading: boolean = false;

  private _placesSubscription: Subscription;
  private _alertEl: HTMLIonAlertElement;

  constructor(
    private _alertController: AlertController,
    private _menuController: MenuController,
    private _store: Store<AppState>
  ) {}

  private _dispatchFetchingActions = (): void => {
    this._store.dispatch({ type: PLACES_ACTION_TYPES.FETCH_PLACES });
  };

  ngOnInit() {
    this._dispatchFetchingActions();

    this.isLoading = true;

    this._placesSubscription = this._store
      .select("placesState")
      .pipe(
        tap((placesState: PlacesState) => {
          if (placesState.httpErrorResponseMessage) {
            this._alertController
              .create({
                header: "Error occurred.",
                message: placesState.httpErrorResponseMessage,
                buttons: [
                  {
                    text: "Okay",
                    handler: () => {
                      if (this._alertEl) {
                        this._alertEl.dismiss().then(() => {
                          this._alertEl = undefined;
                        });
                      }
                    },
                  },
                ],
              })
              .then((alertEl: HTMLIonAlertElement) => {
                this._alertEl = alertEl;
                this._alertEl.present();
              });
          }
        }),
        map((placesState: PlacesState) => [...placesState.places])
      )
      .subscribe((places: Place[]) => {
        this.places = places;
        this.isLoading = false;
      });
  }

  ionViewWillEnter() {
    // console.log("OffersPage ionViewWillEnter");
  }

  ionViewDidEnter() {
    this._menuController.swipeGesture(true, MENU_IDS.MAIN_MENU);
    // console.log("OffersPage ionViewDidEnter");
  }

  ionViewWillLeave() {
    // console.log("OffersPage ionViewWillLeave");
  }

  ionViewDidLeave() {
    // console.log("OffersPage ionViewDidLeave");
  }

  ngOnDestroy() {
    if (this._placesSubscription) this._placesSubscription.unsubscribe();
  }
}
