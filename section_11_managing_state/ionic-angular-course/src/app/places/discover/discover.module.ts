import { NgModule } from "@angular/core";

import { IonicModule } from "@ionic/angular";

import { DiscoverPageRoutingModule } from "./discover-routing.module";

import { DiscoverPage } from "./discover.page";
import { SharedModule } from "src/app/shared/shared.module";
import { PlaceItemComponent } from "./place-item/place-item.component";

@NgModule({
  imports: [SharedModule, IonicModule, DiscoverPageRoutingModule],
  declarations: [DiscoverPage, PlaceItemComponent],
})
export class DiscoverPageModule {}
