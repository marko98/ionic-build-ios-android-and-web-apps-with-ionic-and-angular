import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { Place } from "src/app/shared/model/class/place.model-class";
import { ModalController, IonDatetime, AlertController } from "@ionic/angular";
import {
  FormGroup,
  FormControl,
  Validators,
  AbstractControl,
} from "@angular/forms";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/store/store";
import {
  BOOKINGS_ACTION_TYPES,
  HttpPostBookingAction,
} from "src/app/bookings/store/bookings.actions";
import { Booking } from "src/app/shared/model/class/booking.model-class";
import { take, tap } from "rxjs/operators";
import { AuthState } from "src/app/auth/store/auth.reducer";
import { from, Subscription } from "rxjs";
import { BookingsState } from "src/app/bookings/store/bookings.reducer";
import { Actions, ofType } from "@ngrx/effects";

@Component({
  selector: "app-place-booking",
  templateUrl: "./place-booking.component.html",
  styleUrls: ["./place-booking.component.scss"],
})
export class PlaceBookingComponent implements OnInit, OnDestroy {
  @Input() place: Place;
  @Input() mode: "select" | "random" = "select";
  public formGroup: FormGroup;

  private _bookingsStateSubscription: Subscription;
  private _alertEl: HTMLIonAlertElement;
  private _addBookingsSubscription: Subscription;

  constructor(
    private _modalController: ModalController,
    private _store: Store<AppState>,
    private _alertController: AlertController,
    private _actions$: Actions
  ) {}

  public onDateFromBlur = (): void => {
    let dateTo: AbstractControl = this.formGroup.get("dateTo");
    let dateFrom: AbstractControl = this.formGroup.get("dateFrom");

    if (dateFrom.value && !dateTo) {
      this.formGroup.addControl(
        "dateTo",
        new FormControl(null, {
          updateOn: "blur",
          validators: [Validators.required],
        })
      );
    } else if (dateFrom.value && dateTo && dateFrom.value) {
      if (new Date(dateFrom.value) > new Date(dateTo.value)) {
        this.formGroup.get("dateTo").setValue(undefined);
      }
    }
  };

  public onReduceMsInDate = (date: Date | string, ms: number): Date => {
    if (!(date instanceof Date)) date = new Date(date);

    return new Date(date.getTime() - ms);
  };

  public onAddMsInDate = (date: Date | string, ms: number): Date => {
    if (!(date instanceof Date)) date = new Date(date);

    return new Date(date.getTime() + ms);
  };

  public onSubmit = (): void => {
    if (this.formGroup.valid) {
      // console.log(this.formGroup);

      this._store
        .select("authState")
        .pipe(take(1))
        .subscribe((authState: AuthState) => {
          this._store.dispatch(
            new HttpPostBookingAction({
              userId: authState.userId,
              placeTitle: this.place.title,
              placeId: this.place.id,
              guestNumber: this.formGroup.value["guestNumber"],
              dateFromUTCISOString: new Date(
                new Date(this.formGroup.value["dateFrom"]).toUTCString()
              ).toISOString(),
              dateToUTCISOString: new Date(
                new Date(this.formGroup.value["dateTo"]).toUTCString()
              ).toISOString(),
              userFirstName: this.formGroup.value["firstName"],
              userLastName: this.formGroup.value["lastName"],
              placeImgUrl: this.place.imageUrl,
            })
          );
        });
    }
  };

  public onModalClose = (role: string): void => {
    this._modalController.dismiss(
      { message: "hello, modal is dismissed" },
      role
    );
  };

  ngOnInit() {
    this._bookingsStateSubscription = this._store
      .select("bookingsState")
      .subscribe((bookingsState: BookingsState) => {
        // console.log(bookingsState.httpErrorResponseMessage);
        if (bookingsState.httpErrorResponseMessage)
          from(
            this._alertController.create({
              header: "Error occurred",
              message: bookingsState.httpErrorResponseMessage,
              keyboardClose: true,
              buttons: [
                {
                  text: "Okay",
                  handler: () => {
                    if (this._alertEl) {
                      this._alertEl.dismiss();
                      this._alertEl = undefined;
                    }
                  },
                },
              ],
            })
          )
            .pipe(take(1))
            .subscribe((alertEl: HTMLIonAlertElement) => {
              this._alertEl = alertEl;
              this._alertEl.present();
            });
      });

    this._addBookingsSubscription = this._actions$
      .pipe(
        ofType(BOOKINGS_ACTION_TYPES.ADD_BOOKING),
        tap(() => {
          this._modalController.dismiss(
            { message: "hello, modal is closed" },
            "success"
          );
        })
      )
      .subscribe();

    let dateFrom;
    let dateTo;

    if (this.mode === "random") {
      // console.log(this.place);
      dateFrom = new Date(
        this.place.dateFrom.getTime() +
          Math.random() *
            (this.place.dateTo.getTime() -
              7 * 24 * 60 * 60 * 1000 -
              this.place.dateFrom.getTime())
      );
      dateTo = new Date(
        dateFrom.getTime() + Math.random() * 6 * 24 * 60 * 60 * 1000
      );

      dateFrom = (dateFrom as Date).toISOString();
      dateTo = (dateTo as Date).toISOString();
    }

    this.formGroup = new FormGroup({
      firstName: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      lastName: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      guestNumber: new FormControl(2, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      dateFrom: new FormControl(dateFrom, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
    });

    if (this.mode === "random") {
      this.formGroup.addControl(
        "dateTo",
        new FormControl(dateTo, {
          updateOn: "blur",
          validators: [Validators.required],
        })
      );
    }
    console.log("PlaceBookingComponent init");
  }

  ngOnDestroy() {
    if (this._bookingsStateSubscription)
      this._bookingsStateSubscription.unsubscribe();

    if (this._addBookingsSubscription)
      this._addBookingsSubscription.unsubscribe();
    console.log("PlaceBookingComponent destroyed");
  }
}
