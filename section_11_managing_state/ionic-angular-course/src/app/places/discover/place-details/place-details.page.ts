import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ViewContainerRef,
} from "@angular/core";
import {
  NavController,
  ModalController,
  MenuController,
  ViewWillEnter,
  ViewWillLeave,
  ViewDidEnter,
  ViewDidLeave,
  IonContent,
  ActionSheetController,
} from "@ionic/angular";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { PlaceBookingComponent } from "./place-booking/place-booking.component";
import { Place } from "src/app/shared/model/class/place.model-class";
import { OverlayEventDetail } from "@ionic/core/dist/types/utils/overlays-interface";
import { MENU_IDS } from "src/app/shared/service/menus.service";
import { NavigationService } from "src/app/shared/service/navigation.service";
import { CustomSwipeFunctions } from "src/app/shared/model/interface/custom-swipe-functions.model-interface";
import { Store } from "@ngrx/store";
import { PlacesState } from "../../store/places.reducer";
import { take, map } from "rxjs/operators";
import { AppState } from "src/app/store/store";
import { HttpFetchPlaceAction } from "../../store/places.actions";
import { Subscription } from "rxjs";
import { AuthState } from "src/app/auth/store/auth.reducer";

@Component({
  selector: "app-place-details",
  templateUrl: "./place-details.page.html",
  styleUrls: ["./place-details.page.scss"],
})
export class PlaceDetailsPage
  implements
    OnInit,
    OnDestroy,
    ViewWillEnter,
    ViewWillLeave,
    ViewDidEnter,
    ViewDidLeave {
  public place: Place;

  public customSwipeFns: CustomSwipeFunctions = {
    swipeRight: {
      function: () => {
        console.log("swipeRight", this.customSwipeFns.htmlEl);
        // this._navigationService.onNavigateBackward();
      },
      preventDefault: true,
    },
    swipeDown: {
      function: () => {
        console.log("swipeDown", this.customSwipeFns.htmlEl);
      },
      preventDefault: false,
    },
  };

  public isLoading: boolean = false;
  public userId: number;

  private _placesStateSubscription: Subscription;

  constructor(
    private _router: Router,
    private _navController: NavController,
    private _navigationService: NavigationService,
    private _modalController: ModalController,
    private _activatedRoute: ActivatedRoute,
    private _menuController: MenuController,
    private _actionSheetController: ActionSheetController,
    private _store: Store<AppState>
  ) {}

  public onBookPlace = (): void => {
    // this._router.navigate(["../"]);
    // this._navController.navigateBack(["../"]);
    // this._navController.pop(); // samo 'skine' stranicu(Page) sa steka stranica -> nije bas bezbedan zbog refresh-a na browser-u

    this._actionSheetController
      .create({
        header: "Choose an Action",
        keyboardClose: true,
        buttons: [
          {
            text: "Select Date",
            handler: () => {
              this._onCreateAndOpenModal("select");
            },
          },
          {
            text: "Random Date",
            handler: () => {
              this._onCreateAndOpenModal("random");
            },
          },
          {
            text: "Cancel",
            role: "cancel",
          },
        ],
      })
      .then((ionActionSheetEl: HTMLIonActionSheetElement) => {
        ionActionSheetEl.present();
      });
  };

  private _onCreateAndOpenModal = (mode: "select" | "random"): void => {
    this._modalController
      .create({
        component: PlaceBookingComponent,
        componentProps: {
          place: this.place,
          mode: mode,
        },
      })
      .then((elModal: HTMLIonModalElement) => {
        elModal
          .onDidDismiss()
          .then((overlayEventDetail: OverlayEventDetail<any>) => {
            console.log(overlayEventDetail.data, overlayEventDetail.role);
          });
        elModal.present();
      });
  };

  ngOnInit() {
    this._store
      .select("authState")
      .pipe(take(1))
      .subscribe((authState: AuthState) => {
        this.userId = authState.userId;

        this._activatedRoute.paramMap
          .pipe(take(1))
          .subscribe((paramMap: ParamMap) => {
            if (!paramMap.has("placeId"))
              return this._navController.navigateBack(["../"]);

            this._placesStateSubscription = this._store
              .select("placesState")
              .pipe
              // take(1),
              // map((placesState: PlacesState) => [...placesState.places])
              ()
              .subscribe((placesState: PlacesState) => {
                this.place = placesState.places.find(
                  (p) => p.id === +paramMap.get("placeId")
                );

                if (!this.place && !placesState.fetchedThePlace) {
                  this.isLoading = true;
                  this._store.dispatch(
                    new HttpFetchPlaceAction(+paramMap.get("placeId"))
                  );
                  // return this._navController.navigateBack(["../"]);
                } else if (!this.place && placesState.fetchedThePlace) {
                  this.place = placesState.fetchedThePlace;
                  this.isLoading = false;
                  this._placesStateSubscription.unsubscribe();
                }
              });
          });
      });

    // console.log("PlaceDetailsPage init");
  }

  ionViewWillEnter() {
    // console.log("DiscoverPage ionViewWillEnter");
  }

  ionViewDidEnter() {
    this._menuController.swipeGesture(false, MENU_IDS.MAIN_MENU);

    // console.log("DiscoverPage ionViewDidEnter");
  }

  ionViewWillLeave() {
    // console.log("DiscoverPage ionViewWillLeave");
  }

  ionViewDidLeave() {
    // console.log("DiscoverPage ionViewDidLeave");
  }

  ngOnDestroy() {
    if (this._placesStateSubscription)
      this._placesStateSubscription.unsubscribe();
    // console.log("PlaceDetailsPage destroyed");
  }
}
