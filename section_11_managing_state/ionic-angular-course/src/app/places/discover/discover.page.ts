import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  AfterViewInit,
} from "@angular/core";
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from "@angular/cdk/drag-drop";
import { Place } from "src/app/shared/model/class/place.model-class";
import { Subscription } from "rxjs";
import {
  MenuController,
  NavController,
  ViewWillEnter,
  ViewDidEnter,
  ViewWillLeave,
  ViewDidLeave,
  AlertController,
} from "@ionic/angular";
import { MENU_IDS } from "src/app/shared/service/menus.service";
import { Store } from "@ngrx/store";
import { PlacesState } from "../store/places.reducer";
import { map, take, tap } from "rxjs/operators";
import { AppState } from "src/app/store/store";
import { PLACES_ACTION_TYPES } from "../store/places.actions";
import { AuthState } from "src/app/auth/store/auth.reducer";

enum IonSegmentValues {
  ALL_PLACES = "allPlaces",
  BOOKABLE_PLACES = "bookablePlaces",
}

@Component({
  selector: "app-discover",
  templateUrl: "./discover.page.html",
  styleUrls: ["./discover.page.scss"],
})
export class DiscoverPage
  implements
    OnInit,
    AfterViewInit,
    ViewWillEnter,
    ViewDidEnter,
    ViewWillLeave,
    ViewDidLeave,
    OnDestroy {
  // todo = [
  //   "Get to work",
  //   "Pick up groceries",
  //   "Go home",
  //   "Fall asleep",
  //   "Get to work",
  //   "Pick up groceries",
  //   "Go home",
  //   "Fall asleep",
  // ];

  // done = [
  //   "Get up",
  //   "Brush teeth",
  //   "Take a shower",
  //   "Check e-mail",
  //   "Walk dog",
  //   "Get up",
  //   "Brush teeth",
  //   "Take a shower",
  //   "Check e-mail",
  //   "Walk dog",
  // ];
  public places: Place[];
  public placesForVirtualScroll: Place[];
  public isLoading: boolean = false;
  public activeSegment: IonSegmentValues = IonSegmentValues.ALL_PLACES;

  private _placesSubscription: Subscription;
  private _alertEl: HTMLIonAlertElement;
  private _userId: number;

  constructor(
    private _menuController: MenuController,
    private _navController: NavController,
    private _store: Store<AppState>,
    private _alertController: AlertController
  ) {}

  // drop(event: CdkDragDrop<string[]>) {
  //   if (event.previousContainer === event.container) {
  //     moveItemInArray(
  //       event.container.data,
  //       event.previousIndex,
  //       event.currentIndex
  //     );
  //   } else {
  //     transferArrayItem(
  //       event.previousContainer.data,
  //       event.container.data,
  //       event.previousIndex,
  //       event.currentIndex
  //     );
  //   }
  // }

  private _dispatchFetchingActions = (): void => {
    this._store.dispatch({ type: PLACES_ACTION_TYPES.FETCH_PLACES });
  };

  public onIonSegmentChange = (
    event: CustomEvent<{ value: IonSegmentValues }>
  ): void => {
    if (event.detail.value === IonSegmentValues.BOOKABLE_PLACES) {
      this.placesForVirtualScroll = [...this.places].filter(
        (p) => p.userId !== this._userId
      );
    } else {
      this.placesForVirtualScroll = [...this.places].splice(1);
    }

    this.activeSegment = event.detail.value;
  };

  public onOpenMenu = (menuId: string): void => {
    // enabling one menu will also automatically disable all the others that are on the same side
    this._menuController
      .enable(true, menuId)
      .then((elMenu: HTMLIonMenuElement) => {
        this._menuController.open(menuId);
      });
  };

  ngOnInit() {
    this._dispatchFetchingActions();

    this.isLoading = true;

    this._store
      .select("authState")
      .pipe(take(1))
      .subscribe((authState: AuthState) => {
        this._userId = authState.userId;

        this._placesSubscription = this._store
          .select("placesState")
          .pipe(
            tap((placesState: PlacesState) => {
              if (placesState.httpErrorResponseMessage) {
                this._alertController
                  .create({
                    header: "Error occurred.",
                    message: placesState.httpErrorResponseMessage,
                    buttons: [
                      {
                        text: "Okay",
                        handler: () => {
                          if (this._alertEl) {
                            this._alertEl.dismiss().then(() => {
                              this._alertEl = undefined;
                            });
                          }
                        },
                      },
                    ],
                  })
                  .then((alertEl: HTMLIonAlertElement) => {
                    this._alertEl = alertEl;
                    this._alertEl.present();
                  });
              }
            }),
            map((placesState: PlacesState) => [...placesState.places])
          )
          .subscribe((places: Place[]) => {
            this.places = places;
            this.placesForVirtualScroll = [...this.places].splice(1);
            this.isLoading = false;
          });
      });

    // console.log("DiscoverPage init");
  }

  ngAfterViewInit(): void {
    // console.log("DiscoverPage ngAfterViewInit");
  }

  ionViewWillEnter() {
    // console.log("DiscoverPage ionViewWillEnter");
  }

  ionViewDidEnter() {
    this._menuController.swipeGesture(true, MENU_IDS.MAIN_MENU);
    // console.log("DiscoverPage ionViewDidEnter");
  }

  ionViewWillLeave() {
    // console.log("DiscoverPage ionViewWillLeave");
  }

  ionViewDidLeave() {
    // console.log("DiscoverPage ionViewDidLeave");
  }

  ngOnDestroy(): void {
    if (this._placesSubscription) this._placesSubscription.unsubscribe();
    // console.log("DiscoverPage destroyed");
  }
}
