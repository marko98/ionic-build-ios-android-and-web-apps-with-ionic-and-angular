import { Component, OnInit, OnDestroy } from "@angular/core";

@Component({
  selector: "app-places",
  templateUrl: "./places.page.html",
  styleUrls: ["./places.page.scss"],
})
export class PlacesPage implements OnInit, OnDestroy {
  constructor() {}

  ngOnInit() {
    // console.log("PlacesPage init");
  }

  ngOnDestroy() {
    // console.log("PlacesPage destroyed");
  }
}
